TO DO LIST
==========

Current
-------

Bugs
----

Next
----

Later
-----

Allow gutters to be more flexible. e.g. markers such as break points, bookmarks.
    Right align the line numbers.
Send mouse buttons events, so that items can be toggled on/off.
Need "isLineVisible" and use it to call invalidate/rebuild when items are toggled.

Create an outline gutter for the right, which show where highlight ranges are
    e.g. the selection and the search results.
    Also for git diffs (though IntelliJ puts those on the left AND right).
