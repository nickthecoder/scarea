module uk.co.nickthecoder.scarea.extra {
    requires kotlin.stdlib;
    requires java.prefs;
    requires javafx.graphics;
    requires javafx.controls;
    requires uk.co.nickthecoder.scarea.core;

    exports uk.co.nickthecoder.scarea.extra;
    exports uk.co.nickthecoder.scarea.extra.ui;
    exports uk.co.nickthecoder.scarea.extra.util;
}
