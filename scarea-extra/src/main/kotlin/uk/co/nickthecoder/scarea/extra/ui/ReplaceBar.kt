/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea.extra.ui

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.ToggleButton
import javafx.scene.control.ToolBar
import javafx.scene.control.Tooltip
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import uk.co.nickthecoder.scarea.extra.util.loadGraphic
import uk.co.nickthecoder.scarea.extra.util.onSceneAvailable

/**
 * An example GUI for use with a [ScareaMatcher].
 *
 * Add [toolBar] to your scene.
 * You will also need a [FindBar] to go with this [ReplaceBar].
 */
class ReplaceBar(val matcher: ScareaMatcher) {

    val replacement = HistoryComboBox(replacementHistory).apply {
        promptText = "replacement"
        styleClass.add("replacement")
    }

    val replace = Button("_Replace").apply {
        styleClass.add("replace")
        disableProperty().bind(matcher.matchSelectedProperty().not())
        onAction = EventHandler { matcher.replace(replacement.editor.text ?: "") }
    }

    val replaceAll = Button("Replace _All").apply {
        styleClass.add("replaceAll")
        disableProperty().bind(matcher.matchSelectedProperty().not())
        onAction = EventHandler { matcher.replaceAll(replacement.editor.text ?: "") }
    }

    val toolBar = ToolBar().apply {
        styleClass.add("scarea-replace")
        items.addAll(replacement, replace, replaceAll)
        addEventFilter(KeyEvent.KEY_PRESSED) { keyPressed(it) }
    }


    init {

        matcher.inUseProperty().addListener { _, _, newValue ->
            if (!newValue) {
                toolBar.isVisible = false
            }
        }

        toolBar.visibleProperty().addListener { _, _, newValue ->
            if (newValue) {
                matcher.inUse = true
            }
        }
    }

    private fun keyPressed(event: KeyEvent) {
        if (event.code == KeyCode.ESCAPE) {
            event.consume()
            matcher.inUse = false
        }
    }

    /**
     * This is a work-around for a bug in ComboBox.
     * I want to focus on [find], but doing find.requestFocus causes the caret to be hidden.
     * See https://stackoverflow.com/questions/40239400/javafx-8-missing-caret-in-switch-editable-combobox
     */
    fun requestFocus() {
        replacement.onSceneAvailable {
            replacement.requestFocus()
            Platform.runLater {
                replacement.editor.selectAll()
            }
        }
    }

    fun createToggleButton(): ToggleButton {
        val button = ToggleButton()
        with(button) {
            loadGraphic(FindBar::class.java, "replace.png")
            selectedProperty().bindBidirectional(toolBar.visibleProperty())
            tooltip = Tooltip("Find and Replace")
        }
        return button
    }

    companion object {
        @JvmStatic
        val replacementHistory: ObservableList<String> = FXCollections.observableList(mutableListOf<String>())
    }
}