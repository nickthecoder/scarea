/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea.extra

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.stage.Stage
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.StyleHighlight
import uk.co.nickthecoder.scarea.extra.ui.FindBar
import uk.co.nickthecoder.scarea.extra.ui.GotoDialog
import uk.co.nickthecoder.scarea.extra.ui.ReplaceBar
import uk.co.nickthecoder.scarea.extra.ui.ScareaMatcher
import uk.co.nickthecoder.scarea.extra.util.RemoveHiddenChildren

class ExtraDemo : Application() {

    override fun start(stage: Stage) {
        ExtraDemoGUI(stage).stage.show()
    }


    class ExtraDemoGUI(val stage: Stage) {

        private val scarea = Scarea(INITIAL_TEXT).apply {
            displayLineNumbers = true
        }

        /**
         * The non-gui part controlling the Find/Replace bars.
         */
        private val matcher = ScareaMatcher(scarea)

        /**
         * A tool bar, which appears below the tabPane (inside findAndReplaceToolBars)
         */
        private val findBar = FindBar(matcher)

        /**
         * A tool bar, which appears below the findBar (inside findAndReplaceToolBars)
         */
        private val replaceBar = ReplaceBar(matcher)

        /**
         * At the bottom of the scene. Contains findBar and replaceBar.
         */
        private val findAndReplaceToolBars = VBox().apply {
            // Automatically removes children when they are made invisible.
            // Then replaces them if they are made visible again.
            // Without this, the findAndReplaceToolBars VBox would take up space even when its children were hidden.
            RemoveHiddenChildren(children)
            children.addAll(findBar.toolBar, replaceBar.toolBar)
        }

        private val borderPane = BorderPane().apply {
            center = scarea
            bottom = findAndReplaceToolBars
            addEventHandler(KeyEvent.KEY_PRESSED) { onKeyPressed(scarea, it) }
        }


        init {
            matcher.inUse = false

            stage.scene = Scene(borderPane).apply {
                Scarea.style(this)
            }

            with(stage) {
                width = 800.0
                height = 500.0
                title = "Scarea Extra Demo"
                centerOnScreen()
            }

            scarea.document.addHighlightRanges(listOf(
                    HighlightRange(ScareaPosition(3, 0), ScareaPosition(3, 6), shortcutHighlight, transient = false),
                    HighlightRange(ScareaPosition(4, 0), ScareaPosition(4, 6), shortcutHighlight, transient = false),
                    HighlightRange(ScareaPosition(5, 0), ScareaPosition(5, 6), shortcutHighlight, transient = false)
            ))
        }

        private fun onKeyPressed(scarea: Scarea, event: KeyEvent) {
            if (event.isShortcutDown) {
                if (event.code == KeyCode.K) {
                    // Delete from caret position to end of line
                    val pos = scarea.caretPosition
                    val end = ScareaPosition(pos.line, Int.MAX_VALUE)
                    scarea.document.delete(pos, end)
                }
                if (event.code == KeyCode.G) {
                    GotoDialog(scarea).show()
                    event.consume()
                }
                if (event.code == KeyCode.F) {
                    matcher.inUse = true
                    findBar.requestFocus()
                }
                if (event.code == KeyCode.R) {
                    val wasInUse = matcher.inUse
                    replaceBar.toolBar.isVisible = true
                    if (wasInUse) {
                        replaceBar.requestFocus()
                    } else {
                        findBar.requestFocus()
                    }
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(ExtraDemo::class.java, *args)
        }

        val shortcutHighlight = StyleHighlight("-fx-fill: #008800;")
    }

}

private val INITIAL_TEXT = """Scarea ExtraDemo
A simple demo application showing the features of the "extra" sub project.

ctrl+F = Find
ctrl+R = Replace
ctrl+G = Go to Line

Bye!"""
