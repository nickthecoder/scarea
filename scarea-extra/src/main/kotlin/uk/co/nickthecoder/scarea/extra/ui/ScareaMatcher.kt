/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea.extra.ui

import javafx.beans.property.*
import javafx.beans.value.ChangeListener
import uk.co.nickthecoder.scarea.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Performs find and replace operations on a [Scarea].
 * This has no gui elements, but as it extensively uses JavaFX Properties, then it is quite simple to add a gui
 * layer on top of this matcher.
 *
 * This uses regular expression on [ScareaDocument.text], and will therefore be slow for really long documents.
 * If you have really long documents, a better approach would be to search individual lines using [ScareaDocument.paragraphs]
 * lazily.
 */
class ScareaMatcher(control: Scarea) {

    private val scareaProperty = SimpleObjectProperty<Scarea>(control)
    var scarea: Scarea
        get() = scareaProperty.get()
        set(v) {
            scareaProperty.set(v)
        }

    fun scareaProperty(): ObjectProperty<Scarea> = scareaProperty


    private val findProperty = SimpleStringProperty("")
    var find: String?
        get() = findProperty.get()
        set(v) {
            findProperty.set(v)
        }

    fun findProperty(): StringProperty = findProperty


    private val matchCaseProperty = SimpleBooleanProperty(false)
    var matchCase: Boolean
        get() = matchCaseProperty.get()
        set(v) {
            matchCaseProperty.set(v)
        }

    fun matchCaseProperty(): BooleanProperty = matchCaseProperty


    private val matchRegexProperty = SimpleBooleanProperty(false)
    var matchRegex: Boolean
        get() = matchRegexProperty.get()
        set(v) {
            matchRegexProperty.set(v)
        }

    fun matchRegexProperty(): BooleanProperty = matchRegexProperty


    private val matchWordsProperty = SimpleBooleanProperty(false)
    var matchWords: Boolean
        get() = matchWordsProperty.get()
        set(v) {
            matchWordsProperty.set(v)
        }

    fun matchWordsProperty(): BooleanProperty = matchWordsProperty


    private val statusProperty = SimpleStringProperty("")
    var status: String
        get() = statusProperty.get()
        set(v) {
            statusProperty.set(v)
        }

    fun statusProperty(): StringProperty = statusProperty


    private val hasNextProperty = SimpleBooleanProperty(false)
    var hasNext: Boolean
        get() = hasNextProperty.get()
        set(v) {
            hasNextProperty.set(v)
        }

    fun hasNextProperty(): BooleanProperty = hasNextProperty


    private val hasPrevProperty = SimpleBooleanProperty(false)
    var hasPrev: Boolean
        get() = hasNextProperty.get()
        set(v) {
            hasPrevProperty.set(v)
        }

    fun hasPrevProperty(): BooleanProperty = hasPrevProperty


    private val matchSelectedProperty = SimpleBooleanProperty(false)
    var matchSelected: Boolean
        get() = matchSelectedProperty.get()
        set(v) {
            matchSelectedProperty.set(v)
        }

    fun matchSelectedProperty(): BooleanProperty = matchSelectedProperty


    private val selectionChangedListener = ChangeListener<Pair<ScareaPosition, ScareaPosition>> { _, _, _ ->
        selectionChanged()
    }

    /**
     * When the find bar is hidden, set this to false.
     * When [inUse] == true, then matches will be restarted whenever the text changes, so
     * it will be needless inefficient to keep inUse = true for longer than needed.
     */
    private val inUseProperty = SimpleBooleanProperty(false)
    var inUse: Boolean
        get() = inUseProperty.get()
        set(v) {
            inUseProperty.set(v)
        }

    fun inUseProperty(): BooleanProperty = inUseProperty


    /**
     * Determines how matched results will look.
     */
    var matchHighlight: Highlight = FillStyleHighlight("-fx-fill: black;", "-fx-fill: yellow;")

    /**
     * Determines how replacements will look.
     */
    var replacementHighlight: Highlight = FillStyleHighlight("-fx-fill: black;", "-fx-fill: #ccffcc;")


    private var pattern: Pattern = Pattern.compile("")

    private var matcher: Matcher = pattern.matcher("")

    /**
     * Parts of the document that match
     */
    protected val matches = mutableListOf<HighlightRange>()

    /**
     * Parts of the document which have been REPLACED, i.e. they no longer match, but may still be of
     * interest. [ScareaMatcher] highlights replacements in a different colour from those matches which have
     * not (yet) been replaced.
     */
    private val replacements = mutableListOf<HighlightRange>()

    private var currentMatchIndex = -1

    var maxMatches = 400


    init {
        scareaProperty.addListener { _, oldValue, newValue -> controlChanged(oldValue, newValue) }
        findProperty.addListener { _, _, _ -> startFind() }
        matchCaseProperty.addListener { _, _, _ -> startFind() }
        matchRegexProperty.addListener { _, _, _ -> startFind() }
        matchWordsProperty.addListener { _, _, _ -> startFind() }
        inUseProperty.addListener { _, _, newValue ->
            if (newValue) {
                scarea.selectionProperty().addListener(selectionChangedListener)
            } else {
                scarea.selectionProperty().removeListener(selectionChangedListener)
                clearMatches()
            }
        }
    }

    private fun controlChanged(oldValue: Scarea?, newValue: Scarea?) {
        clearMatches()
        oldValue?.document?.removeRangesByOwner(this)
        oldValue?.selectionProperty()?.removeListener(selectionChangedListener)
        newValue?.selectionProperty()?.addListener(selectionChangedListener)
        startFind(false)
    }

    private fun selectionChanged() {
        val selection = scarea.selection
        currentMatchIndex = findMatchIndex(selection.first, selection.second)
        matchSelected = currentMatchIndex >= 0

        updatePrevNext()
    }

    fun startFind(changeSelection: Boolean = true) {
        clearMatches()
        currentMatchIndex = -1

        if (inUse && find?.isNotEmpty() == true) {

            val caseFlag = if (matchCase) 0 else Pattern.CASE_INSENSITIVE
            val literalFlag = if (matchRegex) 0 else Pattern.LITERAL
            pattern = if (matchWords) {
                if (matchRegex) {
                    Pattern.compile("\\b$find\\b", caseFlag + literalFlag)
                } else {
                    Pattern.compile("\\b${Pattern.quote(find)}\\b", caseFlag)
                }
            } else {
                Pattern.compile(find, caseFlag + literalFlag + Pattern.MULTILINE)
            }
            matcher = pattern.matcher(scarea.text)
            val caret = scarea.caretPosition
            resetIndexToPosition()
            while (matcher.find()) {
                val startPosition = indexToPosition(matcher.start())
                addMatch(startPosition, indexToPosition(matcher.end()))
                if (startPosition >= caret && currentMatchIndex < 0) {
                    currentMatchIndex = matches.size - 1
                }
                if (matches.size >= maxMatches) {
                    break
                }
            }
            // If no matches were found AFTER the caret position, use the FIRST match (if there is one).
            if (currentMatchIndex < 0) {
                currentMatchIndex = 0
            }
            if (changeSelection) {
                highlightCurrentMatch()
            }
        }
        updatePrevNext()
    }

    private fun clearMatches() {
        scarea.document.removeRangesByOwner(this)
        matches.clear()
        replacements.clear()
        currentMatchIndex = -1
        updatePrevNext()
        resetIndexToPosition()
    }

    private var currentLine = 0
    private var currentLineIndex = 0

    private fun resetIndexToPosition() {
        currentLine = 0
        currentLineIndex = 0
    }

    fun indexToPosition(index: Int): ScareaPosition {
        if (currentLineIndex > index) {
            resetIndexToPosition()
        }
        do {
            val end = currentLineIndex + scarea.document.paragraphs[currentLine].length + 1
            if (end > index) {
                return ScareaPosition(currentLine, index - currentLineIndex)
            }
            currentLine++
            currentLineIndex = end
        } while (currentLine < scarea.document.lineCount)

        throw IllegalArgumentException("Index $index is out of range (cl=$currentLine cli=$currentLineIndex)")
    }

    private fun addMatch(start: Int, end: Int): HighlightRange {
        return addMatch(indexToPosition(start), indexToPosition(end))
    }

    private fun addMatch(start: ScareaPosition, end: ScareaPosition): HighlightRange {
        val match = HighlightRange(start, end, matchHighlight, transient = true, owner = this)
        matches.add(match)
        scarea.document.addHighlightRange(match)
        return match
    }

    private fun removeMatch(index: Int): HighlightRange {
        val toRemove = matches.removeAt(index)
        scarea.document.removeHighlightRange(toRemove)
        return toRemove
    }

    private fun addReplacement(start: ScareaPosition, end: ScareaPosition): HighlightRange {
        val hr = HighlightRange(start, end, replacementHighlight, transient = true, owner = this)
        replacements.add(hr)
        scarea.document.addHighlightRange(hr)

        return hr
    }

    private fun updatePrevNext() {
        if (currentMatchIndex < 0) {
            hasPrev = matches.isNotEmpty() && scarea.caretPosition > matches[0].from
            hasNext = matches.isNotEmpty() && scarea.caretPosition < matches.last().from
        } else {
            hasPrev = currentMatchIndex > 0
            hasNext = currentMatchIndex < matches.size - 1
        }

        status = if (find?.isEmpty() != false) {
            ""
        } else if (inUse) {
            if (matches.isEmpty()) {
                "No matches"
            } else if (matches.size == 1) {
                "One match"
            } else if (matches.size >= maxMatches) {
                "Too many matches"
            } else {
                if (currentMatchIndex >= 0) {
                    "${currentMatchIndex + 1} of ${matches.size} matches"
                } else {
                    "${matches.size} matches"
                }
            }
        } else {
            "idle"
        }
    }

    fun previousMatch(loop: Boolean = false) {
        // If we aren't AT one of the matches, then find the previous one based on the caret's position.
        if (currentMatchIndex < 0) {
            val caret = scarea.caretPosition
            for (i in 0 until matches.size) {
                val match = matches[i]
                if (match.from >= caret) {
                    break
                }
                currentMatchIndex = i
            }
        } else {
            if (currentMatchIndex > 0) {
                currentMatchIndex--
            } else if (loop && matches.size > 1) {
                currentMatchIndex = matches.size - 1
            }
        }
        highlightCurrentMatch()
        updatePrevNext()
    }

    /**
     * @param [loop] Go to the first match if we've reached the bottom
     */
    fun nextMatch(loop: Boolean = false) {
        // If we aren't AT one of the matches, then find the next one based on the caret's position.
        if (currentMatchIndex < 0) {
            val caret = scarea.caretPosition
            for (i in 0 until matches.size) {
                val match = matches[i]
                if (match.from >= caret) {
                    currentMatchIndex = i
                    break
                }
            }
        } else {
            if (currentMatchIndex < matches.size - 1) {
                currentMatchIndex++
            } else if (loop && matches.size > 1) {
                currentMatchIndex = 0
            }
        }
        highlightCurrentMatch()
        updatePrevNext()
    }

    /**
     * Highlight the current match. This changes the control's selection.
     */
    private fun highlightCurrentMatch() {
        if (currentMatchIndex >= 0 && currentMatchIndex < matches.size) {
            val match = matches[currentMatchIndex]
            scarea.setSelection(match.to, match.from)
            scarea.ensureCaretVisible()
        }
    }

    fun replace(replacement: String) {
        if (currentMatchIndex >= 0) {
            val match = matches[currentMatchIndex]
            removeMatch(currentMatchIndex)

            scarea.selectionProperty().removeListener(selectionChangedListener)
            scarea.document.replace(match.from, match.to, replacement)
            scarea.selectionProperty().addListener(selectionChangedListener)

            if (replacement.isNotEmpty()) {
                val replacementList = replacement.split("\n")
                val column = if (replacementList.size == 1) {
                    match.from.column + replacementList[0].length
                } else {
                    replacementList.last().length
                }
                val to = ScareaPosition(match.from.line + replacementList.size - 1, column)
                addReplacement(match.from, to)
            }

            highlightCurrentMatch()
            updatePrevNext()
            if (currentMatchIndex >= matches.size) {
                currentMatchIndex = -1
            }
        }
    }

    fun replaceAll(replacement: String) {
        scarea.document.removeRangesByOwner(this)
        val newReplacements = matches.map {
            HighlightRange(it.from, it.to, replacementHighlight, true, false, this)
        }

        scarea.document.history.beginBatch()

        currentMatchIndex = matches.size - 1
        while (currentMatchIndex >= 0) {
            val match = matches[currentMatchIndex]
            scarea.document.replace(match.from, match.to, replacement)
            currentMatchIndex--
        }
        replacements.addAll(matches)
        matches.clear()
        currentMatchIndex = -1
        updatePrevNext()

        scarea.document.history.endBatch()

        scarea.document.addHighlightRanges(newReplacements)
    }

    fun findMatchIndex(start: ScareaPosition, end: ScareaPosition): Int {
        // Optimisation : Test the current match first, as it is the most likely.
        if (currentMatchIndex >= 0 && currentMatchIndex < matches.size) {
            if (matches[currentMatchIndex].from == start && matches[currentMatchIndex].to == end) {
                return currentMatchIndex
            }
        }

        matches.forEachIndexed { i, m ->
            if (m.from == start && m.to == end) return i
            if (m.from > start) return -1 // Matches are in order, so we can end early
        }
        return -1
    }
}
