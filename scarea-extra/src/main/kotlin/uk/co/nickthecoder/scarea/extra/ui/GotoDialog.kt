/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea.extra.ui

import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import javafx.stage.Modality
import javafx.stage.Stage
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.extra.util.loadGraphic

/**
 * A dialog box, allowing the user to position the caret to a give line.
 * They can also specify a column using the syntax :
 *
 *     line:column
 */
class GotoDialog(private var scarea: Scarea) {

    val label = Label("Line [:Column]")
    val field = TextField()

    val hBox = HBox()

    val borderPane = BorderPane()

    val ok = Button("Ok")
    val cancel = Button("Cancel")

    val buttons = FlowPane()
    val scene = Scene(borderPane)
    val stage = Stage()

    init {
        with(borderPane) {
            center = hBox
            bottom = buttons
        }

        with(ok) {
            onAction = EventHandler { onOk() }
            isDefaultButton = true
        }

        with(cancel) {
            isCancelButton = true
            onAction = EventHandler { onCancel() }
        }

        with(buttons) {
            styleClass.add("scarea-buttons")
            children.addAll(ok, cancel)
        }

        with(hBox) {
            styleClass.add("scarea-form")
            children.addAll(label, field)
        }

        // Initial value is the current position.
        val caretPosition = scarea.caretPosition
        field.text = "${caretPosition.line + 1}:${caretPosition.column + 1}"

        stage.initModality(Modality.APPLICATION_MODAL)
        stage.title = "Go to Line/Column"
        stage.scene = scene

        Scarea.style(scene)
    }


    fun show() {
        stage.show()
        stage.centerOnScreen()
    }

    fun onOk() {
        val split = field.text.split(":")
        val line = split[0].toInt() - 1
        val column = if (split.size > 1) (split[1].toInt() - 1) else 0

        scarea.caretPosition = ScareaPosition(line, column)
        stage.hide()
        scarea.requestFocus()
    }

    fun onCancel() {
        stage.hide()
    }

    companion object {
        /**
         * Note [scarea] is a lambda, which returns a [Scarea], so that the button can be
         * used in applications where it may need to apply to one of many [Scarea]s.
         */
        @JvmStatic
        fun createGotoButton(scarea: () -> Scarea): Button {
            val button = Button()
            button.onAction = EventHandler {
                GotoDialog(scarea()).show()
            }

            button.loadGraphic(GotoDialog::class.java, "goto.png")
            return button
        }

        /**
         * Creates a "Goto" button for a single [Scarea]
         * This is only useful if the button will only be used for a single [Scarea].
         */
        @JvmStatic
        fun createGotoButton(scarea: Scarea) = createGotoButton { scarea }
    }
}
