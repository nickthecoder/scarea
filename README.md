# Scarea (Source Code Area)

A Source Code Area, similar to a TextArea suitable for embedding
inside a JavaFX application.

Scarea is written in Kotlin, but will work fine with Java or any other
JVM based language.

## Changes from Tedi

Scarea is my 2nd attempt at writing a styled text area for JavaFX, the first was
Tedi

Unlike Tedi, Scarea does not extend TextInputControl. This avoids lots of problems
(for example the undo/redo of TextInputControl is horrible and cannot be neatly fixed).

Tedi used Integers to specify positions within the document.
Scarea uses a ScareaPosition class containing a line number and column number.
This makes most tasks more efficient, because internally the document is stored
as a list of strings, not as one big String.

Tedi has more features than Scarea (it supports variable width fonts
and different size fonts within the same document too).
These features came at the cost of making the code much more complex than I needed
(I only wanted a source code editor, not a general purpose text editor).
With complexity come bugs, so I chose to return to basics, and support only the
features that I think are important.

Therefore Scarea is much smaller than Tedi, and (fingers crossed) has far fewer bugs).

Scarea is a complete rewrite (though some code was copy/pasted from Tedi).

## Features

* Styled text - useful for syntax highlighting
* Line number gutter
* Optimised for very long documents.
* Lightweight. Has no external dependencies (other than JavaFX of course).
About 1,100 lines of code.

## Features in the "extra" sub-project

* Find / Replace tool
* Go to Line too
* Matching word highlighter

## Pending Features

* Matching brackets highlighter

## Missing Features

* Only supports fixed width fonts
* Text styles cannot change the size of the font.
* Line wrapping not supported.
* Not optimised for very large numbers of highlight ranges (AKA text styles).
(Even simple operations are O(n), where n = the number of highlight ranges in the document).
* No right-to-left text flow
* No special support for Input methods (mobile devices?)
* No special support for Accessibility

## License

Tedi started out as a fork of JavaFX's TextArea,
and so I have kept with that license :
GPL version 2 (with no option to move to a later version).
It is quite possible that there is no remains of TextArea's code left though!
