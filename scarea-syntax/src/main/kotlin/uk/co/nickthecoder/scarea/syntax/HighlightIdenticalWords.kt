package uk.co.nickthecoder.scarea.syntax

import javafx.application.Platform
import javafx.beans.value.ChangeListener
import uk.co.nickthecoder.scarea.FillStyleClassHighlight
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import java.util.regex.Pattern

/**
 * Whenever the caret position changes, find the word at the caret, and highlight other instances of that word.
 * This is useful while programming, as you can see other uses of a variable/field/class.
 *
 * This is based on regular expressions, and therefore is unable to tell if a variable used in one part of the
 * code is actually the SAME variable used elsewhere.
 */
open class HighlightIdenticalWords(val scarea: Scarea, wait: Long = 100) {

    var myRanges = mutableListOf<HighlightRange>()

    private val listener = propertyChangeDelayedThread(scarea.caretPositionProperty(), wait) {
        if (scarea.caretPosition == scarea.markPosition) {
            caretMoved()
        } else {
            // Turn off identical word highlighting when there's a selection.
            Platform.runLater {
                clear()
            }
        }
    }

    private val focusListener = ChangeListener<Boolean> { _, _, _ -> onFocusChanged() }

    init {
        scarea.focusedProperty().addListener(focusListener)
    }

    protected var isFocused = scarea.isFocused

    open fun onFocusChanged() {
        isFocused = scarea.isFocused
        if (!isFocused) {
            clear()
        }
    }

    /**
     * NOTE, this is NOT run on the JavaFX thread!
     */
    open fun caretMoved() {
        if (!isFocused) {
            return
        }

        val caretPosition = scarea.caretPosition
        val newRanges = mutableListOf<HighlightRange>()

        val word = findWordAtPosition(caretPosition)
        if (word.length > 1) {
            for (l in 0 until scarea.document.lineCount) {
                val text = scarea.document.paragraphs[l]
                val matcher = Pattern.compile("\\b${Pattern.quote(word)}\\b").matcher(text)

                while (matcher.find()) {
                    val start = matcher.start()
                    val end = matcher.end()
                    if (matcher.group() == word) {
                        val range = if (l == caretPosition.line && caretPosition.column >= start && caretPosition.column <= end) {
                            createMatchingRange(l, start, end)
                        } else {
                            createMatchedRange(l, start, end)
                        }
                        newRanges.add(range)
                    }
                }
            }
        }

        Platform.runLater {
            clear()
            // Is there more than one instance of this word?
            if (newRanges.size > 1) {
                scarea.document.addHighlightRanges(newRanges)
                myRanges = newRanges
            }
        }
    }

    open fun createMatchedRange(line: Int, start: Int, end: Int): HighlightRange {
        return HighlightRange(
                ScareaPosition(line, start),
                ScareaPosition(line, end),
                matchedHighlight,
                owner = this)
    }

    open fun createMatchingRange(line: Int, start: Int, end: Int): HighlightRange {
        return HighlightRange(
                ScareaPosition(line, start),
                ScareaPosition(line, end),
                matchingHighlight,
                owner = this)
    }

    open fun findWordAtPosition(position: ScareaPosition): String {
        val text = scarea.document.paragraphs[position.line]
        var start = position.column
        var end = text.length
        for (i in position.column - 1 downTo 0) {
            if (!isWordCharacter(text[i])) {
                start = i + 1
                break
            }
        }
        for (i in position.column until text.length) {
            if (!isWordCharacter(text[i])) {
                end = i
                break
            }
        }
        return text.substring(start, end)
    }

    open fun isWordCharacter(c: Char) = c.isJavaIdentifierPart()

    open fun clear() {
        scarea.document.removeRangesByOwner(this)
        myRanges.clear()
    }

    open fun detach() {
        clear()
        scarea.caretPositionProperty().removeListener(listener)
        scarea.focusedProperty().removeListener(focusListener)
    }

    companion object {
        val matchedHighlight = FillStyleClassHighlight("word-matched")
        val matchingHighlight = FillStyleClassHighlight("word-matching")
    }

}
