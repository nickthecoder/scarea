package uk.co.nickthecoder.scarea.syntax

import javafx.beans.value.ChangeListener
import uk.co.nickthecoder.scarea.*


/**
 * When the caret position is within a [PairedHighlightRange], highlight the other
 * half of the pair.
 */
class HighlightMatchedPairs(
        val scarea: Scarea,
        val pairHighlight: Highlight
) {

    constructor(scarea: Scarea) : this(scarea, FillStyleClassHighlight("syntax-pair"))


    private val myRanges = mutableListOf<HighlightRange>()

    private val caretPositionListener = ChangeListener<ScareaPosition> { _, _, _ -> onCaretPositionChanged() }

    private val focusListener = ChangeListener<Boolean> { _, _, _ -> onFocusChanged() }


    init {
        scarea.caretPositionProperty().addListener(caretPositionListener)
        scarea.focusedProperty().addListener(focusListener)
    }


    private fun onCaretPositionChanged() {
        if (scarea.isFocused) {
            checkPositionAndHighlight()
        }
    }

    private fun checkPositionAndHighlight() {
        val caret = scarea.caretPosition

        clear()
        scarea.document.ranges.filterIsInstance(PairedHighlightRange::class.java).forEach { range ->
            if (caret >= range.from && caret <= range.to) {
                highlightOther(range)
            }
        }
        if (myRanges.isNotEmpty()) {
            scarea.document.addHighlightRanges(myRanges)
        }

    }

    private fun onFocusChanged() {
        if (scarea.isFocused) {
            checkPositionAndHighlight()
        } else {
            clear()
        }
    }

    private fun highlightOther(paired: PairedHighlightRange) {
        val newRangeA = HighlightRange(paired.from, paired.to, pairHighlight, owner = this)
        val newRangeB = HighlightRange(paired.pairedWith.from, paired.pairedWith.to, pairHighlight, owner = this)
        myRanges.add(newRangeA)
        myRanges.add(newRangeB)
    }

    fun clear() {
        scarea.document.removeRangesByOwner(this)
        myRanges.clear()
    }

    fun detach() {
        clear()
        scarea.caretPositionProperty().removeListener(caretPositionListener)
        scarea.focusedProperty().removeListener(focusListener)
    }

}
