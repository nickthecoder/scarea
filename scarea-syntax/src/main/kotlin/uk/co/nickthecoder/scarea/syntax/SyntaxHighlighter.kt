package uk.co.nickthecoder.scarea.syntax

import javafx.application.Platform
import javafx.beans.InvalidationListener
import uk.co.nickthecoder.scarea.*
import java.util.concurrent.CountDownLatch

/**
 * The base class for all syntax highlighters.
 */
abstract class SyntaxHighlighter() {

    /**
     * Some standard highlights. You may add/change as needed. There's no need to remove any though.
     * Most highlights are [StyleClassHighlight], except for "error" which is [FillStyleClassHighlight].
     * You need to use scarea.css within your scene for the styles to manifest.
     */
    val highlights = mutableMapOf<String, Highlight>()

    val exceptionHandler: (Exception) -> Unit = {}

    /**
     * A general purpose error highlight.
     * Used as a fall-back in case a highlight name isn't found in the [highlights] map.
     * Also used by RegexSyntax when parentheses, brackets etc are un-matched.
     */
    var ERROR_HIGHLIGHT = DEFAULT_ERROR_HIGHLIGHT

    init {
        listOf(
            "keyword",
            "number",
            "semicolon",
            "annotation",
            "string",
            "string-exp",
            "comment",
            "command",
            "paren",
            "bracket",
            "brace",
            "angle"
        ).forEach {
            highlights[it] = StyleClassHighlight("syntax-$it")
        }

        listOf("error").forEach {
            highlights[it] = ERROR_HIGHLIGHT //FillStyleClassHighlight("syntax-$it")
        }
        highlights["string-exp"] = StyleHighlight("-fx-underline: true; -fx-fill: #333;")
    }

    protected fun highlight(name: String): Highlight = highlights[name] ?: ERROR_HIGHLIGHT

    /**
     * NOTE. [HighlightRange.owner] should be 'this'.
     */
    abstract fun createRanges(text: String): List<HighlightRange>

    protected open fun applyHighlights(scarea: Scarea) {
        if (Platform.isFxApplicationThread()) {
            applyHighlightsNow(scarea)
        } else {
            val latch = CountDownLatch(1)
            Platform.runLater {
                try {
                    applyHighlightsNow(scarea)
                } catch (e: Exception) {
                    exceptionHandler(e)
                } finally {
                    latch.countDown()
                }
            }
            latch.await()
        }
    }

    // Holds the currently running thread, so that an old one can be interrupted when
    // we want to start a new one.
    private var parsingThread: Thread? = null

    private fun applyHighlightsNow(scarea: Scarea) {

        stopOldThread()
        val text = scarea.text
        parsingThread = Thread {
            try {
                val requiredRanges = createRanges(text)
                Platform.runLater {
                    scarea.document.removeRangesByOwner(this)
                    scarea.document.addHighlightRanges(requiredRanges.filter { it.owner === this })
                    if (requiredRanges.firstOrNull { it.owner !== this } != null) {
                        println("Warning. One or more HighlightRanges with incorrect/missing owner")
                    }
                }
            } catch (e: Exception) {
                Platform.runLater {
                    scarea.document.removeRangesByOwner(this)
                    exceptionHandler(e)
                }
            } finally {
                if (parsingThread === Thread.currentThread()) {
                    parsingThread = null
                }
            }
        }.apply { run() }
    }

    private fun stopOldThread() {
        parsingThread?.let {
            try {
                it.interrupt()
            } catch (e: Exception) {
                // Do nothing
            }
            parsingThread = null
        }
    }

    /**
     * Listen to changes to the scarea's text, and recreates the HighlightRanges whenever the text changes.
     * The parsing is performed in a background thread after a short delay.
     *
     * @param wait Wait for an idle period before apply the highlighting. Default is 1000 ms (a second).
     *
     * @return A change listener, which can be passed to [detach], to stop syntax highlighting.
     */
    open fun attach(scarea: Scarea, wait: Long = 1000): InvalidationListener {

        val listener = propertyChangeDelayedThread(scarea.document.textProperty(), { stopOldThread() }, wait) {
            applyHighlights(scarea)
        }

        // Call it immediately, so that even without being changed, the syntax highlighter will run.
        listener.invalidated(scarea.document.textProperty())

        return listener
    }

    /**
     * Stops syntax highlighting.
     *
     * @param listener The listener returned from attach.
     */
    open fun detach(scarea: Scarea, listener: InvalidationListener) {
        scarea.document.textProperty().removeListener(listener)
        clear(scarea)
    }

    /**
     * Clears the highlighting caused by this highlighter.
     */
    open fun clear(scarea: Scarea) {
        scarea.document.removeRangesByOwner(this)
    }

    companion object {
        var DEFAULT_ERROR_HIGHLIGHT = FillStyleHighlight("-fx-fill: black;", "-fx-fill: #ffcccc;")
    }
}
