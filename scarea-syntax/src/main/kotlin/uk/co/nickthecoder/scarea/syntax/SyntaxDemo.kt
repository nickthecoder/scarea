/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea.syntax

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.syntax.groovy.GroovySyntaxHighlighter

class SyntaxDemo : Application() {

    override fun start(stage: Stage) {
        SyntaxDemoGUI(stage).stage.show()
    }

    class SyntaxDemoGUI(val stage: Stage) {

        private val scarea = Scarea(INITIAL_TEXT).apply {
            displayLineNumbers = true
        }


        private val borderPane = BorderPane().apply {
            center = scarea
            addEventHandler(KeyEvent.KEY_PRESSED) { onKeyPressed(scarea, it) }
        }


        init {

            stage.scene = Scene(borderPane).apply {
                Scarea.style(this)
            }
            HighlightIdenticalWords(scarea)
            GroovySyntaxHighlighter.instance.attach(scarea)
            HighlightMatchedPairs(scarea)

            with(stage) {
                width = 800.0
                height = 500.0
                title = "Scarea Syntax Demo"
                centerOnScreen()
            }

        }

        private fun onKeyPressed(scarea: Scarea, event: KeyEvent) {
            if (event.isShortcutDown) {
                if (event.code == KeyCode.K) {
                    // Delete from caret position to end of line
                    val pos = scarea.caretPosition
                    val end = ScareaPosition(pos.line, Int.MAX_VALUE)
                    scarea.document.delete(pos, end)
                }
                // Hit Ctrl+T to see if we can clear all of the highlights.
                // Edit the document, and the highlights will be re-applied.
                if (event.code == KeyCode.T) {
                    GroovySyntaxHighlighter.instance.clear(scarea)
                }
            }
        }

    }

}


fun main(vararg args: String) {
    Application.launch(SyntaxDemo::class.java, *args)
}

private val INITIAL_TEXT = """/**
 * Scarea Syntax Demo
 *
 * A simple demo application showing syntax highlighting for the Groovy language.
 *
 * Comments and strings should use different colors to the main text.
 *
 * When the caret is on a word that appears move than once (such as println), the
 * other instances will be highlighted.
 *
 * While the caret is just before or just after a bracket or curly brace, the matching
 * pair is also highlighted.
 */

class Example {

    // A single line comment
    public int foo() {
        println( "Returning 1" )
        return 1
    }

    /*
        A Multi-line
        comment
    */
    public void bar() {
        if ( true ) { // Click on the "if" and the matching "else" should be highlighted.
            println( "true" )
        } else { // Click on the "else", and the matching "if" should be highlighted
            println( "false" )
        }
    }
}

"""
