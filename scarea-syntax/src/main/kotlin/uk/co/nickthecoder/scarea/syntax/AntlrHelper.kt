package uk.co.nickthecoder.scarea.syntax

import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.Token
import uk.co.nickthecoder.scarea.ScareaPosition


fun ParserRuleContext.startPosition() = start.startPosition()

fun ParserRuleContext.endPosition() = this.stop.endPosition()

fun Token.startPosition() = ScareaPosition(line - 1, charPositionInLine)

fun Token.endPosition(): ScareaPosition {
    val lines = text.split("\n")
    return if (lines.size < 2) {
        ScareaPosition(line - 1, charPositionInLine + text.length)
    } else {
        ScareaPosition(line - 1 + lines.size - 1, lines.last().length)
    }
}
