package uk.co.nickthecoder.scarea.syntax.groovy

import org.antlr.v4.runtime.CharStream
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.ParseTreeWalker
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.syntax.AntlrSyntaxHighlighter

class GroovySyntaxHighlighter : AntlrSyntaxHighlighter() {

    companion object {
        val instance = GroovySyntaxHighlighter()
    }

    override fun createWorker(): Worker = GroovyWorker()

    private inner class GroovyWorker : Worker() {

        override fun createRanges(text: String): List<HighlightRange> {

            val lexer = CommentedGroovyLexer(CharStreams.fromString(text))
            val tokens = CommonTokenStream(lexer)
            val parser = GroovyParser(tokens)

            lexer.removeErrorListeners()
            parser.removeErrorListeners()

            val listener = Listener()
            val tree = parser.compilationUnit()
            ParseTreeWalker.DEFAULT.walk(listener, tree)

            return ranges
        }

        private inner class CommentedGroovyLexer(source: CharStream) : GroovyLexer(source) {

            override fun nextToken(): Token {
                val token = super.nextToken()
                if (token.channel == 1) {
                    highlight(token, "comment")
                }

                return token
            }
        }

        private inner class Listener : GroovyParserBaseListener() {

            override fun visitErrorNode(node: ErrorNode) {
                highlight(node.symbol, "error")
            }

            override fun exitPackageDeclaration(ctx: GroovyParser.PackageDeclarationContext) {
                highlight(ctx.pack) // Package
            }

            override fun exitImportDeclaration(ctx: GroovyParser.ImportDeclarationContext) {
                highlight(ctx.imp) // Import
                highlight(ctx.sta) // Static
            }

            override fun exitModifier(ctx: GroovyParser.ModifierContext) {
                highlight(ctx.m) // native,synchronized,transient,volatile,def,var
            }

            override fun exitClassOrInterfaceModifier(ctx: GroovyParser.ClassOrInterfaceModifierContext) {
                highlight(ctx.m) // public, protected, private, static, abstract, final, strictfp, default
            }

            override fun exitVariableModifier(ctx: GroovyParser.VariableModifierContext) {
                highlight(ctx.m) // final, def, var, public, protected, private, static, abstract,strictfp
            }

            override fun exitTypeParameter(ctx: GroovyParser.TypeParameterContext) {
                highlight(ctx.ex) // extends
                highlight(ctx.cn, "className")
            }

            override fun exitTypeBound(ctx: GroovyParser.TypeBoundContext) {
                highlight(ctx.ba)
            }

            override fun exitClassDeclaration(ctx: GroovyParser.ClassDeclarationContext) {
                highlight(ctx.c) // class, interface, enum, trait
                highlight(ctx.cn, "className")
                highlight(ctx.ex) // extends
                highlight(ctx.im) // implements
            }

            override fun exitClassBody(ctx: GroovyParser.ClassBodyContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitClassBodyDeclaration(ctx: GroovyParser.ClassBodyDeclarationContext) {
                highlight(ctx.sta) // static
            }

            override fun exitReturnType(ctx: GroovyParser.ReturnTypeContext) {
                highlight(ctx.v) // void
            }

            override fun exitPrimitiveType(ctx: GroovyParser.PrimitiveTypeContext) {
                highlight(ctx.prim)
            }

            override fun exitTypeArguments(ctx: GroovyParser.TypeArgumentsContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitTypeArgument(ctx: GroovyParser.TypeArgumentContext) {
                highlight(ctx.eors) // extends, super
            }

            override fun exitFormalParameters(ctx: GroovyParser.FormalParametersContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitThisFormalParameter(ctx: GroovyParser.ThisFormalParameterContext) {
                highlight(ctx.thi)
            }

            override fun exitNullLiteralAlt(ctx: GroovyParser.NullLiteralAltContext) {
                highlight(ctx) // null
            }

            override fun exitFloatingPointLiteralAlt(ctx: GroovyParser.FloatingPointLiteralAltContext) {
                highlight(ctx, "number")
            }

            override fun exitIntegerLiteralAlt(ctx: GroovyParser.IntegerLiteralAltContext?) {
                highlight(ctx, "number")
            }

            override fun exitBooleanLiteralAlt(ctx: GroovyParser.BooleanLiteralAltContext?) {
                highlight(ctx) // true, false
            }

            override fun exitStringLiteral(ctx: GroovyParser.StringLiteralContext) {
                highlight(ctx, "string")
            }

            override fun exitGstring(ctx: GroovyParser.GstringContext) {
                highlight(ctx, "string")
            }

            override fun exitGstringValue(ctx: GroovyParser.GstringValueContext) {
                highlightPair(ctx.left, ctx.right)
                highlight(ctx.exp ?: ctx, "string-exp")
            }

            override fun exitClosure(ctx: GroovyParser.ClosureContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitAnnotation(ctx: GroovyParser.AnnotationContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitElementValueArrayInitializer(ctx: GroovyParser.ElementValueArrayInitializerContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitBlock(ctx: GroovyParser.BlockContext) {
                highlightPair(ctx.left, ctx.right, "brace")
            }

            override fun exitTypeNamePairs(ctx: GroovyParser.TypeNamePairsContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitVariableNames(ctx: GroovyParser.VariableNamesContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitIfElseStatement(ctx: GroovyParser.IfElseStatementContext) {
                if (ctx.i != null && ctx.e != null) {
                    highlightPair(ctx.i, ctx.e, "keyword")
                } else {
                    highlight(ctx.i) // if
                    highlight(ctx.e) // else
                }
            }

            override fun exitSwitchStatement(ctx: GroovyParser.SwitchStatementContext) {
                highlight(ctx.sw) // switch
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitForStmtAlt(ctx: GroovyParser.ForStmtAltContext) {
                highlight(ctx.f)
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitWhileStmtAlt(ctx: GroovyParser.WhileStmtAltContext) {
                highlight(ctx.w) // while
            }

            override fun exitDoWhileStmtAlt(ctx: GroovyParser.DoWhileStmtAltContext) {
                highlightPair(ctx.d, ctx.w, "keyword")
            }

            override fun exitContinueStatement(ctx: GroovyParser.ContinueStatementContext) {
                highlight(ctx.c) // continue
            }

            override fun exitBreakStatement(ctx: GroovyParser.BreakStatementContext) {
                highlight(ctx.b) // break
            }

            override fun exitTryCatchStatement(ctx: GroovyParser.TryCatchStatementContext) {
                highlight(ctx.t) // try
            }

            override fun exitAssertStatement(ctx: GroovyParser.AssertStatementContext) {
                highlight(ctx.ass) // assert
            }

            override fun exitSynchronizedStmtAlt(ctx: GroovyParser.SynchronizedStmtAltContext) {
                highlight(ctx.sy) // synchronized
            }

            override fun exitReturnStmtAlt(ctx: GroovyParser.ReturnStmtAltContext) {
                highlight(ctx.re) // return
            }

            override fun exitThrowStmtAlt(ctx: GroovyParser.ThrowStmtAltContext) {
                highlight(ctx.th) // throw
            }

            override fun exitCatchClause(ctx: GroovyParser.CatchClauseContext) {
                highlight(ctx.ca) // catch
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitFinallyBlock(ctx: GroovyParser.FinallyBlockContext) {
                highlight(ctx.fi) // finally
            }

            override fun exitResources(ctx: GroovyParser.ResourcesContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitSwitchLabel(ctx: GroovyParser.SwitchLabelContext) {
                highlight(ctx.ca) // case
                highlight(ctx.de) // default
            }

            override fun exitEnhancedForControl(ctx: GroovyParser.EnhancedForControlContext) {
                highlight(ctx.i) // in
            }

            override fun exitCastParExpression(ctx: GroovyParser.CastParExpressionContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitExpressionInPar(ctx: GroovyParser.ExpressionInParContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

            override fun exitRelationalExprAlt(ctx: GroovyParser.RelationalExprAltContext) {
                highlight(ctx.op) // as, instanceOf, !instanceOf
            }

            override fun exitIndexPropertyArgs(ctx: GroovyParser.IndexPropertyArgsContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitNamedPropertyArgs(ctx: GroovyParser.NamedPropertyArgsContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitNewPrmrAlt(ctx: GroovyParser.NewPrmrAltContext) {
                highlight(ctx.ne) // new
            }

            override fun exitThisPrmrAlt(ctx: GroovyParser.ThisPrmrAltContext) {
                highlight(ctx.th)
            }

            override fun exitSuperPrmrAlt(ctx: GroovyParser.SuperPrmrAltContext) {
                highlight(ctx.su)
            }

            override fun exitMap(ctx: GroovyParser.MapContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitArrayInitializer(ctx: GroovyParser.ArrayInitializerContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitNonWildcardTypeArguments(ctx: GroovyParser.NonWildcardTypeArgumentsContext) {
                highlightPair(ctx.left, ctx.right)
            }

            override fun exitArguments(ctx: GroovyParser.ArgumentsContext) {
                highlightPair(ctx.left, ctx.right?.right)
            }

        }

    }

}
