package uk.co.nickthecoder.scarea.syntax

import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.tree.TerminalNode
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.PairedHighlightRange

abstract class AntlrSyntaxHighlighter : SyntaxHighlighter() {

    override fun createRanges(text: String): List<HighlightRange> {
        return createWorker().createRanges(text)
    }

    protected abstract fun createWorker(): Worker

    protected abstract inner class Worker {

        protected val ranges = mutableListOf<HighlightRange>()

        fun highlightPair(tokenA: Token?, tokenB: Token?) {
            tokenA ?: return
            tokenB ?: return

            val highlightName = when (tokenA.text) {
                "\"", "\"\"\"" -> "string"
                "{" -> "brace"
                "[" -> "bracket"
                "<" -> "angle"
                "\${" -> "brace"
                else -> "paren"
            }
            highlightPair(tokenA, tokenB, highlightName)
        }

        fun highlightPair(tokenA: Token?, tokenB: Token?, highlightName: String) {
            tokenA ?: return
            tokenB ?: return

            val a = PairedHighlightRange(tokenA.startPosition(), tokenA.endPosition(), highlights[highlightName]!!, transient = true, owner = this@AntlrSyntaxHighlighter, opening = null)
            val b = PairedHighlightRange(tokenB.startPosition(), tokenB.endPosition(), highlights[highlightName]!!, transient = true, owner = this@AntlrSyntaxHighlighter, opening = a)

            ranges.add(a)
            ranges.add(b)
        }

        fun highlightPair(nodeA: TerminalNode?, nodeB: TerminalNode?) {
            nodeA ?: return
            nodeB ?: return

            highlightPair(nodeA.symbol, nodeB.symbol)
        }

        fun highlightPair(nodeA: TerminalNode?, nodeB: TerminalNode?, highlightName: String) {
            nodeA ?: return
            nodeB ?: return

            highlightPair(nodeA.symbol, nodeB.symbol, highlightName)
        }

        fun highlight(ctx: ParserRuleContext?, highlightName: String = "keyword") {
            ctx ?: return
            val highlight = highlights[highlightName] ?: return
            ranges.add(HighlightRange(ctx.startPosition(), ctx.endPosition(), highlight, transient = true, owner = this@AntlrSyntaxHighlighter))
        }

        fun highlight(token: Token?, highlightName: String = "keyword") {
            token ?: return
            ranges.add(HighlightRange(token.startPosition(), token.endPosition(), highlights[highlightName]!!, transient = true, owner = this@AntlrSyntaxHighlighter))
        }

        fun highlight(node: TerminalNode?, highlightName: String = "keyword") {
            node ?: return
            highlight(node.symbol, highlightName)
        }

        abstract fun createRanges(text: String): List<HighlightRange>

    }
}
