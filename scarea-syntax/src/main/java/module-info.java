module uk.co.nickthecoder.scarea.syntax {
    requires kotlin.stdlib;
    requires javafx.graphics;
    requires javafx.controls;
    requires uk.co.nickthecoder.scarea.core;
    requires org.antlr.antlr4.runtime;

    exports uk.co.nickthecoder.scarea.syntax;
    exports uk.co.nickthecoder.scarea.syntax.groovy;
}
