module uk.co.nickthecoder.scarea.core {
    requires kotlin.stdlib;
    requires javafx.graphics;
    requires javafx.controls;

    exports uk.co.nickthecoder.scarea;
}
