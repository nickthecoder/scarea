/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea

interface ScareaListener {
    /**
     * @param document The document that was altered
     * @param from The start position where text was altered
     * @param to The end of the newly added text
     * @param removedTo Where the end of the removed text was
     *
     * Note, [removedTo] may be useful, in conjunction with [to] to adjust markers etc,
     * but it shouldn't be used to access parts of the document (as it may refer to an invalid position).
     */
    fun replaced(document: ScareaDocument, from: ScareaPosition, to: ScareaPosition, removedTo: ScareaPosition) {}

    fun rangesAdded(document: ScareaDocument, ranges: List<HighlightRange>) {}

    fun rangesRemoved(document: ScareaDocument, ranges: List<HighlightRange>) {}
}
