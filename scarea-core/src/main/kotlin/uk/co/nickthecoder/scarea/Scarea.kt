/*
 * A very small amount of this code (maybe zero) may have been copied (and convert from Java to Kotlin)
 * from JavaFX's TextArea. Therefore I have kept TextArea's copyright message.
 *
 * Copyright (c) 2011, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package uk.co.nickthecoder.scarea

import javafx.beans.binding.BooleanBinding
import javafx.beans.binding.IntegerBinding
import javafx.beans.property.*
import javafx.beans.value.ObservableIntegerValue
import javafx.css.CssMetaData
import javafx.css.Styleable
import javafx.css.StyleableBooleanProperty
import javafx.css.StyleableObjectProperty
import javafx.scene.Scene
import javafx.scene.control.Control
import javafx.scene.control.Skin
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import javafx.scene.text.Font

/**
 * [Scarea] is short for Source Code AREA (pronounced scarier).
 * It is similar to JavaFX's TextArea, but can also contain styled text.
 * For example, it can be used as a code editor, where the source code is highlighted using different
 * colors and/or text styles.
 * Unlike TextArea, [Scarea] only works with fixed width fonts (such as Courier).
 *
 * It has been optimised for large documents, and unlike a TextArea, the document is not stored as a
 * single String, so [text] is a calculated value, which is slow for large documents. Use it sparingly!
 *
 * Basic operations, such as insert and delete), will be O(n) where n is the numbers of [HighlightRange]s,
 * so [Scarea] isn't suitable for documents with a very large number of highlights.
 *
 * In the simplest case there is a 1 to 1 relationship between a [Scarea] and a [ScareaDocument].
 * However, you can create a split view, by creating a second (third or forth etc) [Scarea] which shares
 * the same [ScareaDocument].
 *
 * Many operations are only available through the [ScareaDocument].
 * For example, to undo a change, you would call :
 *
 *      myScarea.document.history.undo()
 *
 * A [Scarea] has a [caretPosition] (which is the position of the flashing caret). as well as a
 * [markPosition].
 * When selecting text [caretPosition] and [markPosition] are the two ends of the selection.
 * When there is no selection [markPosition] == [caretPosition].
 * Note, [caretPosition] may be before or after [markPosition].
 *
 * [caretPosition] and [markPosition] are NOT part of the [ScareaDocument], and therefore undo/redo will NOT
 * include simple changes to those positions.
 */
class Scarea(val document: ScareaDocument) : Control() {

    constructor() : this(ScareaDocument(""))

    constructor(str: String) : this(ScareaDocument(str))

    //---------------------------------------------------------------------------
    // Fields
    //---------------------------------------------------------------------------

    /**
     * A representation of the document as a single [String].
     * This is a calculated value, which is expensive in memory and time, so use it sparingly.
     * This calls [ScareaDocument.text].
     *
     * Consider using [ScareaDocument.insert], [ScareaDocument.delete] and [ScareaDocument.replace] to modify a document, and
     * [ScareaDocument.substring], [ScareaDocument.part] or [ScareaDocument.paragraphs] to read part of a document.
     *
     * This differs from [ScareaDocument.text]. When you assign a new value, the caret position is set to the beginning of the
     * document, and the undo [ScareaDocument.History] is cleared (so you cannot undo).
     * (This is designed to make it easy to set text when a document is first loaded).
     */
    var text: String
        get() = document.text
        set(v) {
            document.text = v
            caretPosition = ScareaPosition(0, 0)
            document.history.clear()
            ensureCaretVisible()
        }

    private val documentListener = object : ScareaListener {
        override fun replaced(
            document: ScareaDocument,
            from: ScareaPosition,
            to: ScareaPosition,
            removedTo: ScareaPosition
        ) {
            // Adjust the selection (or just the caret) when text is added/deleted/replaced.
            setSelection(
                document.adjustPosition(markPosition, from, to, removedTo),
                document.adjustPosition(caretPosition, from, to, removedTo)
            )
        }
    }

    //---------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------

    /**
     * If you attempt to assign an invalid position, then it is clamped to a valid value without throwing an exception.
     * This includes lines outside the range 0..[ScareaDocument.lineCount] as well as columns outside the range 0..lineLength.
     *
     * Note, when you change the [caretPosition], the [markPosition] is also changed (clearing any previous
     * selection). If you wish to keep the selection use [setSelection] instead.
     *
     * Property [caretPositionProperty].
     */
    var caretPosition: ScareaPosition
        get() = caretPositionProperty.value
        set(v) {
            if (markPosition != caretPositionProperty.value) {
                scareaSkin?.forceRebuild()
            }
            caretPositionProperty.value = document.safe(v)
            markPosition = v
            selectionProperty.value = selection
        }
    private val caretPositionProperty = SimpleObjectProperty(ScareaPosition.START)
    fun caretPositionProperty(): ReadOnlyObjectProperty<ScareaPosition> = caretPositionProperty


    /**
     * Selected text is defined by two positions [caretPosition] and [markPosition].
     * They can be in either order (i.e. [caretPosition] >= [markPosition] or [caretPosition] < [markPosition])
     *
     * See [selection] (which has [caretPosition] and [markPosition] in numerical order).
     *
     * Property [markPositionProperty].
     */
    var markPosition: ScareaPosition
        get() = markPositionProperty.value
        set(v) {
            if (v != markPositionProperty.value) {
                markPositionProperty.value = document.safe(v)
                selectionProperty.value = selection
                scareaSkin?.forceRebuild()
            }
        }
    private val markPositionProperty = SimpleObjectProperty(ScareaPosition.START)
    fun markPositionProperty(): ReadOnlyObjectProperty<ScareaPosition> = markPositionProperty


    /**
     * Combines the [caretPosition] and the [markPosition] into a [Pair], where the first item is
     * the smaller of the two.
     *
     * Property [selectionProperty].
     */
    val selection: Pair<ScareaPosition, ScareaPosition>
        get() = Pair(min(caretPosition, markPosition), max(caretPosition, markPosition))

    private val selectionProperty = SimpleObjectProperty(selection)
    fun selectionProperty(): ReadOnlyObjectProperty<Pair<ScareaPosition, ScareaPosition>> = selectionProperty

    /**
     * Is there selected text?
     */
    val hasSelection: Boolean
        get() = hasSelectionProperty.value
    private val hasSelectionProperty = caretPositionProperty.isNotEqualTo(markPositionProperty)
    fun hasSelectionProperty(): BooleanBinding = hasSelectionProperty

    /**
     * The line of the [caretPosition].
     * This is zero based (i.e. at the top of the document, line = 0).
     *
     * Property [lineProperty]. See [incremented].
     */
    val line: Int
        get() = caretPosition.line

    fun lineProperty(): ObservableIntegerValue = lineProperty

    private val lineProperty by lazy {
        object : IntegerBinding() {
            init {
                bind(caretPositionProperty)
            }

            override fun computeValue() = caretPosition.line
        }
    }


    /**
     * The column of the [caretPosition].
     * This is zero based (i.e. at the left of the document, column = 0).
     *
     * Property [columnProperty]. See [incremented].
     */
    val column: Int
        get() = caretPosition.column

    fun columnProperty(): ObservableIntegerValue = lineProperty

    private val columnProperty by lazy {
        object : IntegerBinding() {
            init {
                bind(caretPositionProperty)
            }

            override fun computeValue() = caretPosition.column
        }
    }


    /**
     * Determines if line numbers are displayed.
     * This can also be set using the css : -fx-display-line-numbers
     *
     * Property [displayLineNumbersProperty].
     */
    var displayLineNumbers: Boolean
        get() = displayLineNumbersProperty.get()
        set(v) = displayLineNumbersProperty.set(v)
    private val displayLineNumbersProperty: StyleableBooleanProperty =
        createStyleable("displayLineNumbers", false, DISPLAY_LINE_NUMBERS)

    fun displayLineNumbersProperty(): BooleanProperty = displayLineNumbersProperty


    /**
     * Property [fontProperty].
     */
    var font: Font
        get() = fontProperty.get()
        set(v) = fontProperty.set(v)
    private val fontProperty: StyleableObjectProperty<Font> = createStyleable("font", Font.getDefault(), FONT)
    fun fontProperty(): ObjectProperty<Font> = fontProperty

    /**
     * Property [editableProperty].
     */
    var editable: Boolean
        get() = editableProperty.value
        set(v) = editableProperty.set(v)
    private val editableProperty = SimpleBooleanProperty(true).apply {
        addListener { _, _, newValue ->
            styleClass.removeIf { it == "read-only" }
            if (newValue == false) {
                styleClass.add("read-only")
            }
        }
    }

    fun editableProperty(): BooleanProperty = editableProperty

    //---------------------------------------------------------------------------
    // init
    //---------------------------------------------------------------------------

    init {
        // Note, in order to use the same styles as a regular JavaFX's TextArea we include text-area.
        styleClass.addAll("text-area", "scarea")

        document.addListener(documentListener)
    }

    //---------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------

    fun positionForPoint(x: Double, y: Double): ScareaPosition = scareaSkin?.positionForPoint(x, y)
        ?: ScareaPosition.START

    fun setSelection(mark: ScareaPosition, caret: ScareaPosition) {
        val needsRebuild = !(markPosition == caretPosition && caret == mark)

        caretPositionProperty.value = document.safe(caret)
        markPositionProperty.value = document.safe(mark)

        selectionProperty.value = selection

        if (needsRebuild) {
            scareaSkin?.forceRebuild()
        }
    }

    fun ensureCaretVisible() = scareaSkin?.ensureCaretVisible()


    fun selectedText() = document.substring(min(markPosition, caretPosition), max(markPosition, caretPosition))

    fun replaceSelection(str: String) {
        val (start, end) = selection
        document.replace(start, end, str)
    }

    /**
     * Counts the number of spaces at the beginning of the line.
     */
    private fun leadingSpaceCount(line: String): Int {
        var spaceCount = 0
        for (c in line) {
            if (c == ' ') {
                spaceCount++
            } else {
                break
            }
        }
        return spaceCount
    }

    /**
     * Replaces tab characters at the start of the line, with appropriate number of spaces characters,
     * using document.indentSize.
     */
    private fun replaceLeadingTabs(line: String): String {
        var tabCount = 0
        for (c in line) {
            if (c == '\t') {
                tabCount++
            } else {
                break
            }
        }
        return if (tabCount == 0) line else " ".repeat(document.indentSize * tabCount) + line.substring(tabCount)
    }

    /**
     * When the TAB key is pressed, indent the currently selected lines.
     * If there is no selection, indent the current line
     * (irrespective of where the caret it - i.e. it can be in the middle of a line!)
     *
     * The selection should contain the same text (ignoring whitespace) should be the same before and after.
     */
    fun indent() {

        val fromLine = Math.min(caretPosition.line, markPosition.line)
        val toLine = Math.max(caretPosition.line, markPosition.line)
        val oldFromColumn = if (caretPosition.line == fromLine) caretPosition.column else markPosition.column
        val oldToColumn = if (caretPosition.line == toLine) caretPosition.column else markPosition.column
        val fromLineLength = document.paragraphs[fromLine].length
        val toLineLength = document.paragraphs[toLine].length

        val selectedLines = document.paragraphs.subList(fromLine, toLine + 1)
        setSelection(ScareaPosition(fromLine, 0), ScareaPosition(toLine, Int.MAX_VALUE))

        val replacement = selectedLines.joinToString(separator = "\n") { indentLine(it) }
        replaceSelection(replacement)

        val fromColumn = oldFromColumn + document.paragraphs[fromLine].length - fromLineLength
        val toColumn = oldToColumn + document.paragraphs[toLine].length - toLineLength
        setSelection(ScareaPosition(fromLine, fromColumn), ScareaPosition(toLine, toColumn))

    }

    /**
     * When the Shift-TAB key is pressed, reduce the indent of the currently selected lines.
     * If there is no selection, unindent the current line
     * (irrespective of where the caret it - i.e. it can be in the middle of a line!)
     *
     * The selection should contain the same text (ignoring whitespace) should be the same before and after.
     */
    fun unindent() {

        val fromLine = Math.min(caretPosition.line, markPosition.line)
        val toLine = Math.max(caretPosition.line, markPosition.line)
        val oldFromColumn = if (caretPosition.line == fromLine) caretPosition.column else markPosition.column
        val oldToColumn = if (caretPosition.line == toLine) caretPosition.column else markPosition.column
        val fromLineLength = document.paragraphs[fromLine].length
        val toLineLength = document.paragraphs[toLine].length

        val selectedLines = document.paragraphs.subList(fromLine, toLine + 1)
        setSelection(ScareaPosition(fromLine, 0), ScareaPosition(toLine, Int.MAX_VALUE))

        val replacement = selectedLines.joinToString(separator = "\n") { unindentLine(it) }
        replaceSelection(replacement)

        val fromColumn = oldFromColumn + document.paragraphs[fromLine].length - fromLineLength
        val toColumn = oldToColumn + document.paragraphs[toLine].length - toLineLength
        setSelection(ScareaPosition(fromLine, fromColumn), ScareaPosition(toLine, toColumn))
    }

    /**
     * Indents a line using document.indentSize.
     * If the line is currently mis-aligned, then it is made aligned by adding an appropriate number of spaces.
     */
    private fun indentLine(origLine: String): String {
        val line = replaceLeadingTabs(origLine)
        val spaceCount = leadingSpaceCount(line)
        val extra = if (spaceCount % document.indentSize == 0) {
            document.indentSize
        } else {
            document.indentSize - (spaceCount % document.indentSize)
        }
        return " ".repeat(extra) + line
    }

    /**
     * Unindents a line using document.indentSize.
     * If the line is currently mis-aligned, then it is made aligned by adding an appropriate number of spaces.
     */
    private fun unindentLine(origLine: String): String {
        val line = replaceLeadingTabs(origLine)
        val spaceCount = leadingSpaceCount(line)
        val sub = if (spaceCount % document.indentSize == 0) {
            document.indentSize
        } else {
            spaceCount % document.indentSize
        }

        return if (spaceCount >= sub) {
            line.substring(sub)
        } else {
            line
        }
    }

    fun copy() {
        val selectedText = selectedText()
        if (selectedText.isNotEmpty()) {
            val content = ClipboardContent()
            content.putString(selectedText)
            Clipboard.getSystemClipboard().setContent(content)
        }
    }

    fun cut() {
        copy()
        replaceSelection("")
    }

    fun paste() {
        val clipboard = Clipboard.getSystemClipboard()
        if (clipboard.hasString()) {
            val text = clipboard.string
            if (text != null) {
                replaceSelection(text)
            }
        }
    }


    override fun createDefaultSkin(): Skin<*> {
        return ScareaSkin(this)
    }

    val scareaSkin
        get() = skin as? ScareaSkin

    override fun getControlCssMetaData(): List<CssMetaData<out Styleable, *>> = getClassCssMetaData()

    //---------------------------------------------------------------------------
    // Companion Object
    //---------------------------------------------------------------------------

    companion object {

        val DISPLAY_LINE_NUMBERS =
            createBooleanCssMetaData<Scarea>("-fx-display-line-numbers") { it.displayLineNumbersProperty }
        val FONT = createFontCssMetaData<Scarea>("-fx-font") { it.fontProperty }

        private val STYLEABLES: List<CssMetaData<out Styleable, *>> = listOf(
            FONT, DISPLAY_LINE_NUMBERS
        )

        fun getClassCssMetaData(): List<CssMetaData<out Styleable, *>> = STYLEABLES

        @JvmStatic
        fun style(scene: Scene?) {
            Scarea::class.java.getResource("scarea.css")?.let {
                scene?.stylesheets?.add(it.toExternalForm())
            }

        }
    }
}
