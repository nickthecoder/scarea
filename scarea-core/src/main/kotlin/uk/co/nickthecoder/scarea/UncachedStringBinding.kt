package uk.co.nickthecoder.scarea

import javafx.beans.InvalidationListener
import javafx.beans.binding.Binding
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableStringValue
import javafx.collections.FXCollections
import uk.co.nickthecoder.scarea.comsun.ExpressionHelper

/**
 * For [ScareaDocument.textProperty], I want an observable value, whose value is not cached (I don't want the
 * text kept in memory!)
 */
abstract class UncachedStringBinding : Binding<String>, ObservableStringValue {

    private var helper: ExpressionHelper<String>? = null

    override fun get() = getValue()

    override fun isValid() = true

    override fun addListener(var1: InvalidationListener?) {
        helper = ExpressionHelper.addListener(helper, this, var1)
    }

    override fun removeListener(var1: InvalidationListener?) {
        helper = ExpressionHelper.removeListener(helper, var1)
    }

    override fun addListener(var1: ChangeListener<in String?>?) {
        helper = ExpressionHelper.addListener(helper, this, var1)
    }

    override fun removeListener(var1: ChangeListener<in String?>?) {
        helper = ExpressionHelper.removeListener(helper, var1)
    }

    override fun invalidate() {
        ExpressionHelper.fireValueChangedEvent(helper)
    }

    override fun dispose() {
    }

    override fun getDependencies() = FXCollections.emptyObservableList<Any>()

}