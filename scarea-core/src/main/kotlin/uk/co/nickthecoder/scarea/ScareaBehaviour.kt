/*
 * A very small amount of this code (maybe zero by now!) may have been copied
 * (and convert from Java to Kotlin).
 * from JavaFX's TextArea. Therefore I have kept TextArea's copyright message.
 * The vast majority of the code was written by Nick Robinson.
 *
 * Copyright (c) 2011, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package uk.co.nickthecoder.scarea

import javafx.scene.input.*
import uk.co.nickthecoder.scarea.comsun.*

class ScareaBehavior(private val control: Scarea)

    : BehaviorBase<Scarea>(control, SCAREA_BINDINGS) {

    private val document = control.document

    /**
     * Used to keep track of the most recent key event. This is used when
     * handling InputCharacter actions.
     */
    private var lastEvent: KeyEvent? = null

    init {
        control.addEventHandler(ScrollEvent.SCROLL) { onScroll(it) }
    }

    private fun moveCaretBy(lines: Int, columns: Int) {
        if (lines != 0) {
            control.caretPosition = ScareaPosition(control.caretPosition.line + lines, control.caretPosition.column)
        }

        if (columns != 0) {
            control.caretPosition = document.addColumns(control.caretPosition, columns)
        }
        control.ensureCaretVisible()
    }

    private fun moveCaretTo(line: Int, column: Int) {
        control.caretPosition = ScareaPosition(line, column)
        control.ensureCaretVisible()
    }

    private fun moveCaretTo(position: ScareaPosition) {
        control.caretPosition = position
        control.ensureCaretVisible()
    }

    private fun selectTo(line: Int, column: Int) {
        control.setSelection(control.markPosition, ScareaPosition(line, column))
        control.ensureCaretVisible()
    }

    private fun selectBy(lines: Int, columns: Int) {
        if (lines != 0) {
            control.setSelection(
                control.markPosition,
                ScareaPosition(control.caretPosition.line + lines, control.caretPosition.column)
            )
        } else {
            if (columns != 0) {
                control.setSelection(control.markPosition, document.addColumns(control.caretPosition, columns))
            }
        }
        control.ensureCaretVisible()
    }

    private fun jumpCategory(c: Char): Int = if (c.isWhitespace()) 0 else if (c.isLetterOrDigit()) 1 else 2

    private fun jumpWord(direction: Int, growSelection: Boolean) {

        if (direction == 0) return

        val oldMark = control.markPosition
        val d = if (direction < 0) -1 else 1

        if (d < 0 && control.caretPosition.line <= 0 && control.caretPosition.column <= 0) return
        if (d > 0 && control.caretPosition.line >= document.lineCount - 1 && control.caretPosition.column >= document.paragraphs.last().length - 1) return

        val first =
            document.charAt(if (d == -1) document.addColumns(control.caretPosition, d) else control.caretPosition)
        val cat = jumpCategory(first)

        while (true) {
            val nextPosition = document.addColumns(control.caretPosition, d)
            if (cat != jumpCategory(document.charAt(if (d == 1) control.caretPosition else nextPosition))) break
            if (nextPosition == control.caretPosition) break
            control.caretPosition = nextPosition
        }

        // The caret has now jumped to the correct place.

        if (growSelection) {
            control.setSelection(oldMark, control.caretPosition)
        }
        control.ensureCaretVisible()
    }

    /**
     * The default handler for a key typed event, which is called when none of
     * the other key bindings match. This is the method which handles basic
     * text entry.
     * @param event not null
     */
    private fun keyTyped(event: KeyEvent) {

        if (!control.editable || control.isDisabled) return

        // Sometimes we get events with no key character, in which case
        // we need to bail.
        val character = event.character
        if (character.isEmpty()) return

        // Filter out control keys except control & Alt on PC or Alt on Mac
        if (event.isControlDown || event.isAltDown || isMac && event.isMetaDown) {
            if (!((event.isControlDown || isMac) && event.isAltDown)) return
        }

        // Ignore characters in the control range and the ASCII delete
        // character as well as meta key presses
        if (character[0].toInt() > 0x1F
            && character[0].toInt() != 0x7F
            && !event.isMetaDown
        ) { // Not sure about this one
            control.replaceSelection(character)
            control.ensureCaretVisible()
        }
    }

    private fun onScroll(event: ScrollEvent) {
        val skin = control.scareaSkin ?: return

        if (skin.vScroll.isVisible && event.deltaY != 0.0) {
            // Is the mouse over the horizontal scroll bar?
            if (skin.hScroll.isVisible && skin.hScroll.contains(skin.hScroll.sceneToLocal(event.x, event.y))) {
                // Do nothing. Let the ScrollBar handle this itself!
            } else {
                event.consume()
                skin.scrollBy(0.0, -event.deltaY)
            }
        }

        if (skin.hScroll.isVisible && event.deltaX != 0.0) {
            event.consume()
            skin.scrollBy(-event.deltaX, 0.0)
        }
    }

    /**
     * Records the last KeyEvent we saw.
     */
    override fun callActionForEvent(e: KeyEvent) {
        lastEvent = e
        super.callActionForEvent(e)
    }

    public override fun callAction(name: String) {

        val skin = control.scareaSkin ?: return

        when (name) {

            // Simple movement
            "Right" -> if (control.caretPosition == control.markPosition) moveCaretBy(
                0,
                1
            ) else moveCaretTo(max(control.caretPosition, control.markPosition))
            "Left" -> if (control.caretPosition == control.markPosition) moveCaretBy(
                0,
                -1
            ) else moveCaretTo(min(control.caretPosition, control.markPosition))
            "Up" -> moveCaretBy(-1, 0)
            "Down" -> moveCaretBy(1, 0)

            "StartOfLine" -> moveCaretTo(control.caretPosition.line, 0)
            "EndOfLine" -> moveCaretTo(control.caretPosition.line, Int.MAX_VALUE)
            "PageUp" -> moveCaretBy(-skin.linesPerPage() + 1, 0)
            "PageDown" -> moveCaretBy(skin.linesPerPage() - 1, 0)
            "StartOfDocument" -> moveCaretTo(0, 0)
            "EndOfDocument" -> moveCaretTo(document.lineCount - 1, Int.MAX_VALUE)

            "WordLeft" -> jumpWord(-1, false)
            "WordRight" -> jumpWord(1, false)

            // Selection
            "SelectRight" -> selectBy(0, 1)
            "SelectLeft" -> selectBy(0, -1)
            "SelectUp" -> selectBy(-1, 0)
            "SelectDown" -> selectBy(1, 0)

            "SelectStartOfLine" -> selectTo(control.caretPosition.line, 0)
            "SelectEndOfLine" -> selectTo(control.caretPosition.line, Int.MAX_VALUE)
            "SelectPageUp" -> selectBy(-skin.linesPerPage() + 1, 0)
            "SelectPageDown" -> selectBy(skin.linesPerPage() - 1, 0)
            "SelectStartOfDocument" -> selectTo(0, 0)
            "SelectEndOfDocument" -> selectTo(document.lineCount - 1, Int.MAX_VALUE)

            "SelectWordLeft" -> jumpWord(-1, true)
            "SelectWordRight" -> jumpWord(1, true)

            "SelectAll" -> control.setSelection(ScareaPosition.START, document.endPosition())
            "SelectNone" -> control.markPosition = control.caretPosition

            "Copy" -> control.copy()
            "Escape" -> control.markPosition = control.caretPosition

            else -> if (control.editable) {

                when (name) {
                    "InputCharacter" -> lastEvent?.let { keyTyped(it) }

                    // Special
                    "NewLine" -> {
                        control.replaceSelection("\n")
                        control.ensureCaretVisible()
                    }
                    "Indent" -> control.indent()
                    "Unindent" -> control.unindent()

                    "DeletePreviousChar" -> if (control.caretPosition == control.markPosition) {
                        control.setSelection(control.caretPosition, document.addColumns(control.caretPosition, -1))
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    } else {
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    }
                    "DeleteNextChar" -> if (control.caretPosition == control.markPosition) {
                        control.setSelection(control.caretPosition, document.addColumns(control.caretPosition, 1))
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    } else {
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    }
                    "DeletePreviousWord" -> if (control.caretPosition == control.markPosition) {
                        jumpWord(-1, true)
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    } else {
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    }
                    "DeleteNextWord" -> if (control.caretPosition == control.markPosition) {
                        jumpWord(1, true)
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    } else {
                        control.replaceSelection("")
                        control.ensureCaretVisible()
                    }

                    "Cut" -> {
                        control.cut()
                        control.ensureCaretVisible()
                    }
                    "Paste" -> {
                        control.paste()
                        control.ensureCaretVisible()
                    }
                    "Undo" -> {
                        document.history.undo()
                        control.ensureCaretVisible()
                    }
                    "Redo" -> {
                        document.history.redo()
                        control.ensureCaretVisible()
                    }

                    else -> super.callAction(name)
                }
            } else {
                super.callAction(name)
            }
        }
    }

    private var lastMouseReleased: Long = 0

    override fun mousePressed(e: MouseEvent) {
        // We never respond to events if disabled
        if (control.isDisabled) return

        if (!control.isFocused) {
            control.requestFocus()
        }

        // Primary button?
        if (e.button == MouseButton.PRIMARY) {

            val mousePosition = control.positionForPoint(e.x, e.y)

            if (!(e.isControlDown || e.isAltDown || e.isShiftDown || e.isMetaDown || e.isShortcutDown)) {
                when (e.clickCount) {
                    1 -> moveCaretTo(mousePosition)
                    2 -> {
                        // Select a word
                        jumpWord(-1, true)
                        val sel = control.selection
                        control.setSelection(sel.first, sel.second)
                        jumpWord(1, true)
                    }
                    3 -> {
                        // Select a line
                        control.setSelection(
                            ScareaPosition(control.caretPosition.line, 0),
                            ScareaPosition(control.caretPosition.line, Int.MAX_VALUE)
                        )
                    }
                }
            } else if (e.isShiftDown && !(e.isControlDown || e.isAltDown || e.isMetaDown || e.isShortcutDown) && e.clickCount == 1) {
                control.setSelection(control.markPosition, mousePosition)
            }
        }

    }

    override fun mouseReleased(e: MouseEvent) {
        if (e.button == MouseButton.PRIMARY) {
            lastMouseReleased = System.currentTimeMillis()
        }
        super.mouseReleased(e)
    }

    override fun mouseDragged(e: MouseEvent) {
        // We never respond to events if disabled
        if (control.isDisabled) return

        if (e.button == MouseButton.PRIMARY) {
            // Ignore drags immediately after a release. I added this because double clicking a word would sometimes select
            // half of it, because the word would get selected, and then a tiny mouse movement would mess it up.
            if (System.currentTimeMillis() - lastMouseReleased > 500) {
                if (!(e.isControlDown || e.isAltDown || e.isShiftDown || e.isMetaDown || e.isShortcutDown)) {
                    val mousePosition = control.positionForPoint(e.x, e.y)
                    selectTo(mousePosition.line, mousePosition.column)
                }
            }
        }
    }


    companion object {

        val SCAREA_BINDINGS = listOf(

            // Simple movement
            KeyBinding(KeyCode.RIGHT, KeyEvent.KEY_PRESSED, "Right"),
            KeyBinding(KeyCode.KP_RIGHT, KeyEvent.KEY_PRESSED, "Right"),
            KeyBinding(KeyCode.LEFT, KeyEvent.KEY_PRESSED, "Left"),
            KeyBinding(KeyCode.KP_LEFT, KeyEvent.KEY_PRESSED, "Left"),
            KeyBinding(KeyCode.UP, KeyEvent.KEY_PRESSED, "Up"),
            KeyBinding(KeyCode.KP_UP, KeyEvent.KEY_PRESSED, "Up"),
            KeyBinding(KeyCode.DOWN, KeyEvent.KEY_PRESSED, "Down"),
            KeyBinding(KeyCode.KP_DOWN, KeyEvent.KEY_PRESSED, "Down"),

            KeyBinding(KeyCode.HOME, KeyEvent.KEY_PRESSED, "StartOfLine"),
            KeyBinding(KeyCode.END, KeyEvent.KEY_PRESSED, "EndOfLine"),
            KeyBinding(KeyCode.PAGE_UP, KeyEvent.KEY_PRESSED, "PageUp"),
            KeyBinding(KeyCode.PAGE_DOWN, KeyEvent.KEY_PRESSED, "PageDown"),
            KeyBinding(KeyCode.HOME, KeyEvent.KEY_PRESSED, "StartOfDocument").shortcut(),
            KeyBinding(KeyCode.END, KeyEvent.KEY_PRESSED, "EndOfDocument").shortcut(),

            KeyBinding(KeyCode.LEFT, KeyEvent.KEY_PRESSED, "WordLeft").shortcut(),
            KeyBinding(KeyCode.KP_LEFT, KeyEvent.KEY_PRESSED, "WordLeft").shortcut(),
            KeyBinding(KeyCode.RIGHT, KeyEvent.KEY_PRESSED, "WordRight").shortcut(),
            KeyBinding(KeyCode.KP_RIGHT, KeyEvent.KEY_PRESSED, "WordRight").shortcut(),


            // selection
            KeyBinding(KeyCode.RIGHT, KeyEvent.KEY_PRESSED, "SelectRight").shift(),
            KeyBinding(KeyCode.KP_RIGHT, KeyEvent.KEY_PRESSED, "SelectRight").shift(),
            KeyBinding(KeyCode.LEFT, KeyEvent.KEY_PRESSED, "SelectLeft").shift(),
            KeyBinding(KeyCode.KP_LEFT, KeyEvent.KEY_PRESSED, "SelectLeft").shift(),
            KeyBinding(KeyCode.UP, KeyEvent.KEY_PRESSED, "SelectUp").shift(),
            KeyBinding(KeyCode.KP_UP, KeyEvent.KEY_PRESSED, "SelectUp").shift(),
            KeyBinding(KeyCode.DOWN, KeyEvent.KEY_PRESSED, "SelectDown").shift(),
            KeyBinding(KeyCode.KP_DOWN, KeyEvent.KEY_PRESSED, "SelectDown").shift(),

            KeyBinding(KeyCode.HOME, KeyEvent.KEY_PRESSED, "SelectStartOfLine").shift(),
            KeyBinding(KeyCode.END, KeyEvent.KEY_PRESSED, "SelectEndOfLine").shift(),
            KeyBinding(KeyCode.PAGE_UP, KeyEvent.KEY_PRESSED, "SelectPageUp").shift(),
            KeyBinding(KeyCode.PAGE_DOWN, KeyEvent.KEY_PRESSED, "SelectPageDown").shift(),
            KeyBinding(KeyCode.HOME, KeyEvent.KEY_PRESSED, "SelectStartOfDocument").shortcut().shift(),
            KeyBinding(KeyCode.END, KeyEvent.KEY_PRESSED, "SelectEndOfDocument").shortcut().shift(),

            KeyBinding(KeyCode.LEFT, KeyEvent.KEY_PRESSED, "SelectWordLeft").shortcut().shift(),
            KeyBinding(KeyCode.KP_LEFT, KeyEvent.KEY_PRESSED, "SelectWordLeft").shortcut().shift(),
            KeyBinding(KeyCode.RIGHT, KeyEvent.KEY_PRESSED, "SelectWordRight").shortcut().shift(),
            KeyBinding(KeyCode.KP_RIGHT, KeyEvent.KEY_PRESSED, "SelectWordRight").shortcut().shift(),

            KeyBinding(KeyCode.A, KeyEvent.KEY_PRESSED, "SelectAll").shortcut(),
            KeyBinding(KeyCode.A, KeyEvent.KEY_PRESSED, "SelectNone").shortcut().shift(),
            KeyBinding(KeyCode.BACK_SLASH, KeyEvent.KEY_PRESSED, "SelectNone").shortcut(),

            // Special
            KeyBinding(KeyCode.ENTER, KeyEvent.KEY_PRESSED, "NewLine"),
            KeyBinding(KeyCode.TAB, KeyEvent.KEY_PRESSED, "Indent"),
            KeyBinding(KeyCode.TAB, KeyEvent.KEY_PRESSED, "Unindent").shift(),
            KeyBinding(KeyCode.ESCAPE, "Escape"),

            // deletion
            KeyBinding(KeyCode.BACK_SPACE, KeyEvent.KEY_PRESSED, "DeletePreviousChar"),
            KeyBinding(KeyCode.BACK_SPACE, KeyEvent.KEY_PRESSED, "DeletePreviousChar").shift(),
            KeyBinding(KeyCode.BACK_SPACE, KeyEvent.KEY_PRESSED, "DeletePreviousWord").shortcut(),
            KeyBinding(KeyCode.H, KeyEvent.KEY_PRESSED, "DeletePreviousChar").shortcut(),
            KeyBinding(KeyCode.DELETE, KeyEvent.KEY_PRESSED, "DeleteNextChar"),
            KeyBinding(KeyCode.DELETE, KeyEvent.KEY_PRESSED, "DeleteNextChar").shift(),
            KeyBinding(KeyCode.DELETE, KeyEvent.KEY_PRESSED, "DeleteNextWord").shortcut(),

            // cut/copy/paste
            KeyBinding(KeyCode.CUT, KeyEvent.KEY_PRESSED, "Cut"),
            KeyBinding(KeyCode.DELETE, KeyEvent.KEY_PRESSED, "Cut").shift(),
            KeyBinding(KeyCode.X, KeyEvent.KEY_PRESSED, "Cut").shortcut(),
            KeyBinding(KeyCode.COPY, KeyEvent.KEY_PRESSED, "Copy"),
            KeyBinding(KeyCode.C, KeyEvent.KEY_PRESSED, "Copy").shortcut(),
            KeyBinding(KeyCode.PASTE, KeyEvent.KEY_PRESSED, "Paste"),
            KeyBinding(KeyCode.V, KeyEvent.KEY_PRESSED, "Paste").shortcut(),
            KeyBinding(KeyCode.INSERT, KeyEvent.KEY_PRESSED, "Paste").shift(),
            KeyBinding(KeyCode.INSERT, KeyEvent.KEY_PRESSED, "Copy").shortcut(),
            KeyBinding(KeyCode.Z, KeyEvent.KEY_PRESSED, "Undo").shortcut(),
            KeyBinding(KeyCode.Z, KeyEvent.KEY_PRESSED, "Redo").shortcut().shift(),
            KeyBinding(KeyCode.Y, KeyEvent.KEY_PRESSED, "Redo").shortcut(),

            // Traversal Bindings
            KeyBinding(KeyCode.TAB, "TraverseNext").shortcut(),
            KeyBinding(KeyCode.TAB, "TraversePrevious").shift().shortcut(),

            KeyBinding(null, KeyEvent.KEY_TYPED, "InputCharacter")
                .alt(OptionalBoolean.ANY)
                .shift(OptionalBoolean.ANY)
                .ctrl(OptionalBoolean.ANY)
                .meta(OptionalBoolean.ANY)

        )


    }
}
