/*
 * A very small amount of this code (maybe zero) may have been copied (and convert from Java to Kotlin)
 * from JavaFX's TextArea. Therefore I have kept TextArea's copyright message.
 *
 * Copyright (c) 2011, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package uk.co.nickthecoder.scarea

import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.application.Platform
import javafx.collections.ObservableList
import javafx.css.CssMetaData
import javafx.css.Styleable
import javafx.css.StyleableObjectProperty
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Orientation
import javafx.geometry.VPos
import javafx.scene.Node
import javafx.scene.control.ScrollBar
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.shape.Rectangle
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.util.Duration
import uk.co.nickthecoder.scarea.comsun.*

/**
 * The skin for [Scarea].
 * Note, you cannot replace this skin with a different implementation;
 * [Scarea] casts its skin for features such as finding the [ScareaPosition] of a pixel coordinate.
 */
class ScareaSkin(val scarea: Scarea)
    : BehaviorSkinBase<Scarea, ScareaBehavior>(scarea, ScareaBehavior(scarea)) {

    //---------------------------------------------------------------------------
    // Fields
    //---------------------------------------------------------------------------

    /**
     * Each item can be mapped to an item in [ScareaDocument.paragraphs].
     * This will only contain the visible items, so will be smaller than [ScareaDocument.paragraphs].
     */
    private val styledLines = mutableListOf<StyledLine>()

    /**
     * The "main" part, containing all of the [Text] and [Rectangle]s
     */
    private val clippedRegion = PlainRegion().apply {
        styleClass.add("clipped")
    }

    private val unclippedRegion = PlainRegion().apply {
        styleClass.add("content")
    }


    private val gutter = PlainRegion().apply {
        styleClass.add("gutter")
    }

    /**
     * The [ScrollBar.value] is measured in pixels.
     */
    internal val hScroll = ScrollBar().apply {
        orientation = Orientation.HORIZONTAL
        unitIncrement = 20.0
    }

    /**
     * The [ScrollBar.value] is measured in pixels.
     */
    internal val vScroll = ScrollBar().apply {
        orientation = Orientation.VERTICAL
        unitIncrement = 20.0
    }

    /**
     * When dirty, the [styledLines] will be recreated. To limit the amount of times [rebuild] is called,
     * [forceRebuild] sets this to true, then performs Platform.runLater, and only calls [rebuild] if dirty
     * is still true. Therefore multiple calls to [forceRebuild] may result in only a single call to [rebuild].
     */
    private var dirty = true

    /**
     * The "empty" area in the bottom right when both scroll bars are visible.
     */
    private var corner = StackPane().apply {
        styleClass.setAll("corner")
    }

    private var caret = Rectangle(2.0, 20.0).apply {
        isManaged = false
        // Hide the caret when the control loses focus.
        visibleProperty().bind(scarea.focusedProperty())
    }

    internal var fontWidth = 20.0

    internal var lineHeight = 20.0

    private var gutterWidth = 0.0

    val marginTop
        get() = unclippedRegion.snappedTopInset()
    val marginRight
        get() = unclippedRegion.snappedRightInset()
    val marginBottom
        get() = unclippedRegion.snappedBottomInset()
    val marginLeft
        get() = unclippedRegion.snappedLeftInset()

    private var linesPerPage = 1

    private val documentListener = object : ScareaListener {
        override fun replaced(document: ScareaDocument, from: ScareaPosition, to: ScareaPosition, removedTo: ScareaPosition) {
            // TODO We can be smarter than this if the change only affects a single line???
            forceRebuild()
        }

        override fun rangesAdded(document: ScareaDocument, ranges: List<HighlightRange>) {
            forceRebuild()
        }

        override fun rangesRemoved(document: ScareaDocument, ranges: List<HighlightRange>) {
            forceRebuild()
        }
    }

    private val caretAnimation = CaretAnimation()

    //---------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------

    /**
     * The fill to use for the text under normal conditions
     */
    var textFill: Paint?
        get() = textFillProperty.get()
        set(v) = textFillProperty.set(v)
    private val textFillProperty: StyleableObjectProperty<Paint?> = createStyleable("textFill", Color.BLACK, TEXT_FILL)

    /**
     * The background behind the selected text
     */
    var highlightFill: Paint?
        get() = highlightFillProperty.get()
        set(v) = highlightFillProperty.set(v)
    private val highlightFillProperty: StyleableObjectProperty<Paint?> = createStyleable("highlightFill", Color.DODGERBLUE, HIGHLIGHT_FILL)

    /**
     * The selected text's color
     */
    var highlightTextFill: Paint?
        get() = highlightTextFillProperty.get()
        set(v) = highlightTextFillProperty.set(v)
    private val highlightTextFillProperty: StyleableObjectProperty<Paint?> = createStyleable("highlightTextFill", Color.WHITE, HIGHLIGHT_TEXT_FILL)

    /**
     * The current line's background color
     */
    var currentLineFill: Paint?
        get() = currentLineFillProperty.get()
        set(v) = currentLineFillProperty.set(v)
    private val currentLineFillProperty: StyleableObjectProperty<Paint?> = createStyleable("currentLineFill", Color.WHITE, CURRENT_LINE_FILL)

    //---------------------------------------------------------------------------
    // Init
    //---------------------------------------------------------------------------

    init {
        unclippedRegion.children.addAll(clippedRegion)
        children.addAll(unclippedRegion, gutter, hScroll, vScroll, corner)
        children.forEach { it.isManaged = false }

        vScroll.valueProperty().addListener { _, _, _ -> forceRebuild() }
        hScroll.valueProperty().addListener { _, _, _ ->
            positionStyledText()
            moveCaret()
        }

        fun fontUpdated(newValue: Font) {
            val bounds = Text("x").apply { font = newValue }.layoutBounds
            lineHeight = bounds.height
            fontWidth = bounds.width
            caret.apply {
                height = lineHeight
            }
        }
        skinnable.fontProperty().addListener { _, _, newValue -> fontUpdated(newValue) }
        fontUpdated(skinnable.font)

        skinnable.displayLineNumbersProperty().addListener { _, _, newValue ->
            gutter.isVisible = newValue
            skinnable.requestLayout()
        }

        clippedRegion.children.add(caret)

        scarea.document.addListener(documentListener)
        moveCaret()
        scarea.caretPositionProperty().addListener { _, _, _ ->
            moveCaret()
        }

        scarea.markPositionProperty().addListener { _, _, _ ->
            // TODO Update if selection changed
        }

        caret.fill = textFill
        textFillProperty.addListener { _, _, newValue -> caret.fill = newValue }
    }

    //---------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------

    fun scrollBy(deltaX: Double, deltaY: Double) {
        vScroll.safeValue += deltaY
        hScroll.safeValue += deltaX
    }

    private fun moveCaret() {
        caret.layoutX = scarea.caretPosition.column * fontWidth - 1 - hScroll.safeValue
        caret.layoutY = scarea.caretPosition.line * lineHeight - vScroll.safeValue + marginTop + caret.height * 0.2
    }

    fun linesPerPage() = linesPerPage

    fun positionForPoint(x: Double, y: Double): ScareaPosition {
        return ScareaPosition(
                ((y - marginTop + vScroll.safeValue - lineHeight / 4) / lineHeight).toInt(),
                ((x - gutterWidth - marginLeft - fontWidth + hScroll.safeValue) / fontWidth).toInt()
        )
    }

    fun ensureCaretVisible() {
        val caretPosition = skinnable.caretPosition

        val scrollY = vScroll.safeValue
        val topLine = scrollY / lineHeight

        if (caretPosition.line < topLine) {
            vScroll.value = scrollY - (topLine - caretPosition.line) * lineHeight
        } else {
            val bottomLine = topLine + clipRect.height / lineHeight - 2
            if (caretPosition.line > bottomLine) {
                vScroll.value = scrollY + (caretPosition.line - bottomLine) * lineHeight
            }
        }

        val scrollX = hScroll.safeValue
        val leftColumn = scrollX / fontWidth

        if (caretPosition.column < leftColumn) {
            hScroll.value = scrollX - (leftColumn - caretPosition.column) * fontWidth
        } else {
            val rightColumn = leftColumn + clipRect.width / fontWidth - 1
            if (caretPosition.column > rightColumn) {
                val newValue = scrollX + (caretPosition.column - rightColumn) * fontWidth
                if (hScroll.max < newValue) {
                    hScroll.max = newValue
                }
                hScroll.value = newValue
            }
        }
    }

    private val clipRect = Rectangle().apply {
        isSmooth = false
    }

    override fun layoutChildren(contentX: Double, contentY: Double, contentWidth: Double, contentHeight: Double) {
        rebuild()
        layoutChildren(contentWidth, contentHeight)
    }

    private fun layoutChildren(contentWidth: Double, contentHeight: Double) {

        val scrollWidth = vScroll.prefWidth(contentHeight)
        val scrollHeight = hScroll.prefHeight(contentWidth)

        val availHeight = if (hScroll.isVisible) {
            contentHeight - scrollHeight
        } else {
            contentHeight
        }
        val contentLeft = if (gutter.isVisible) {
            gutter.snappedLeftInset() + gutter.snappedRightInset() + gutterWidth
        } else {
            0.0
        }
        val availWidth = if (vScroll.isVisible) {
            contentWidth - scrollWidth - contentLeft
        } else {
            contentWidth - contentLeft
        }

        if (hScroll.isVisible) {
            hScroll.resizeRelocate(0.0, availHeight, if (vScroll.isVisible) skinnable.width - scrollWidth else skinnable.width, scrollHeight)
            hScroll.blockIncrement = availWidth / 2.0
        }
        if (vScroll.isVisible) {
            vScroll.resizeRelocate(contentWidth - scrollWidth, 0.0, scrollWidth, availHeight)
            vScroll.blockIncrement = availHeight / 2.0
        }
        if (corner.isVisible) {
            corner.resizeRelocate(contentWidth - scrollWidth, availHeight, scrollWidth, scrollHeight)
        }

        linesPerPage = (availHeight / lineHeight).toInt()

        clippedRegion.resizeRelocate(marginLeft, 0.0, availWidth - marginLeft - marginRight, availHeight)
        unclippedRegion.resizeRelocate(contentLeft, 0.0, availWidth, availHeight)

        clipRect.width = availWidth - marginLeft - marginRight
        clipRect.height = availHeight
        clippedRegion.clip = clipRect
    }

    internal fun forceRebuild() {
        dirty = true
        Platform.runLater { if (dirty) rebuild() }
    }


    private fun rebuild() {
        dirty = false

        val scarea = skinnable
        val document = skinnable.document

        val firstLine = Math.max(0, (vScroll.safeValue / lineHeight).toInt() - 1)
        val lastLine = Math.min(document.lineCount, firstLine + (skinnable.height / lineHeight).toInt() + 2)
        val firstPosition = ScareaPosition(firstLine, 0)
        val lastPosition = ScareaPosition(lastLine, 0)

        // Prepare a map of all of the transitions that affect the visible part of the document.
        // A transition is a ScareaPosition where a HighlightRange starts or ends.
        val transitions = mutableMapOf<ScareaPosition, Transition>(
                firstPosition to Transition(firstPosition), lastPosition to Transition(lastPosition)
        )

        fun findTransition(pos: ScareaPosition): Transition {
            val clamped = pos.clamp(firstPosition, lastPosition)
            return transitions[clamped]
                    ?: (Transition(clamped).apply {
                        transitions[clamped] = this
                    })
        }

        for (r in document.ranges) {
            if (r.to < firstPosition || r.from > lastPosition) {
                // Don't bother including it (won't be visible)
            } else {
                findTransition(r.from).addHighlights.add(r.highlight)
                findTransition(r.to).removeHighlights.add(r.highlight)
            }
        }
        if (scarea.markPosition != scarea.caretPosition) {
            val (from, to) = scarea.selection
            if (to < firstPosition || from > lastPosition) {
                // Don't bother including it (won't be visible)
            } else {
                findTransition(from).addHighlights.add(selectionHighlight)
                findTransition(to).removeHighlights.add(selectionHighlight)
            }
        }

        val ordered = transitions.values.toSortedSet()
        val highlights = mutableListOf<Highlight>()

        styledLines.clear()
        clippedRegion.children.clear()

        // The prep work is done, let's start adding the text!
        var previousPosition = firstPosition
        var styledLine = StyledLine()
        styledLines.add(styledLine)

        for (t in ordered) {

            if (t.position > previousPosition) {
                val part = document.part(previousPosition, t.position)

                for (i in 0 until part.size) {
                    val str = part[i]

                    val styledText = StyledText(str)

                    if (highlights.contains(selectionHighlight)) {
                        // ONLY use the selectionHighlight - all other highlights will be ignored.
                        styledText.applyHighlight(selectionHighlight)
                    } else {
                        for (h in highlights) {
                            styledText.applyHighlight(h)
                        }
                    }

                    styledLine.items.add(styledText)
                    styledText.rectangle?.let { clippedRegion.children.add(it) }
                    clippedRegion.children.add(styledText.text)
                    if (i != part.size - 1) {
                        styledLine = StyledLine()
                        styledLines.add(styledLine)
                    }
                }
            }

            t.addHighlights.forEach { highlights.add(it) }
            t.removeHighlights.forEach { highlights.remove(it) }
            previousPosition = t.position
        }

        gutter.children.clear()
        gutterWidth = 0.0
        if (gutter.isVisible) {
            var y = marginTop + lineHeight + firstLine * lineHeight - vScroll.safeValue

            for (lineNumber in firstLine until lastLine) {
                val gutterText = Text((lineNumber + 1).toString()).apply {
                    styleClass.add("line-number")
                    textOrigin = VPos.BASELINE
                    wrappingWidth = 0.0
                    isManaged = false
                    layoutX = gutter.snappedLeftInset()
                    layoutY = y
                }
                gutter.children.add(gutterText)
                gutterText.applyCss()
                gutterWidth = Math.max(gutterWidth, gutterText.layoutBounds.width)
                y += lineHeight

            }
        }

        positionStyledText()
        clippedRegion.children.add(caret)

        moveCaret()
    }

    private fun positionStyledText() {

        // Pretend the document is longer when deciding whether to show the vertical scroll bar.
        // IntelliJ does this, and I think it gives a pleasant experience.
        // It also prevents problems when the horizontal scroll bar appears. Without this, the appearance of
        // the two scroll bars would depend on each other in a cyclic manner, which is annoying to code, and
        // a less pleasant user experience.
        val extraLines = 3

        val scrollY = vScroll.safeValue
        val scrollX = hScroll.safeValue

        val firstLine = Math.max(0, (scrollY / lineHeight).toInt() - 1)

        // Now position them all.
        var x = 0.0
        var y = marginTop + lineHeight + firstLine * lineHeight
        var maxLineLength = 0
        for (styledLine in styledLines) {
            val ll = styledLine.lineLength()
            if (ll > maxLineLength) {
                maxLineLength = ll
            }

            for (styledText in styledLine.items) {
                x += styledText.position(x - scrollX, y - scrollY)
            }
            x = 0.0
            y += lineHeight
        }
        val maxX = marginLeft + marginRight + maxLineLength * fontWidth

        val vScrollWasVisible = vScroll.isVisible
        val hScrollWasVisible = hScroll.isVisible

        vScroll.setBounds((skinnable.document.lineCount + extraLines) * lineHeight + marginTop + marginBottom, skinnable.height)
        hScroll.setBounds(maxX + marginRight, clipRect.width)
        corner.isVisible = vScroll.isVisible && hScroll.isVisible

        /**
         * If the scroll bars visibility has changed, then we need to layout the children again
         * (and we may currently only be in a rebuild, not a full layoutChildren).
         */
        if (vScrollWasVisible != vScroll.isVisible || hScrollWasVisible != hScroll.isVisible) {
            skinnable.requestLayout()
        }
    }


    //---------------------------------------------------------------------------
    // Additional Classes
    //---------------------------------------------------------------------------

    /**
     * A Highlight used by [selectionHighlightRange] to highlight the selected text.
     *
     * Note, fill colors are BOUND, so that other highlight ranges do not affect them
     * (changes to style and styleClass will have no effect to a bound property).
     *
     * This makes this Highlight more potent than regular Highlights.
     */
    private val selectionHighlight = SelectionHighlight()

    private inner class SelectionHighlight : FillHighlight {
        override fun style(rect: Rectangle) {
            rect.style += "-fx-fill: #${highlightFillProperty.value.toString().substring(2, 8)}"
        }

        override fun style(text: Text) {
            text.style += "-fx-fill: #${highlightTextFillProperty.value.toString().substring(2, 8)}"
        }
    }

    /**
     * Used while drawing - it marks the start/end of a HighlightRange.
     * [addHighlights] are all of the highlights that start at this transition, and
     * [removeHighlights] are all of the highlights that finish at this transition.
     */
    private class Transition(val position: ScareaPosition) : Comparable<Transition> {

        val addHighlights = mutableListOf<Highlight>()
        val removeHighlights = mutableListOf<Highlight>()

        override fun compareTo(other: Transition): Int {
            return position.compareTo(other.position)
        }

        override fun toString() = "$position"
    }

    /**
     * Used by the [clippedRegion] and the [gutter]
     */
    private inner class PlainRegion : Region() {
        init {
            isManaged = false
        }

        // Make it public.
        public override fun getChildren(): ObservableList<Node> = super.getChildren()

        // Children are being manually laid out
        override fun layoutChildren() {
            return
        }
    }

    /**
     * A gui version of a single line of text (an item in [ScareaDocument.paragraphs]).
     * For plain text, this will have a single [StyledText], but if the line is split by one or more
     * [HighlightRange]s, then it will have multiple [StyledText]s.
     */
    private inner class StyledLine() {
        val items = mutableListOf<StyledText>()
        fun lineLength(): Int = items.sumBy { it.text.text.length }
        fun text(): String = items.joinToString(separator = "") { it.text.text }
    }

    private inner class StyledText(str: String) {

        val text = Text(str).apply {
            styleClass.add("text")
            textOrigin = VPos.BASELINE
            wrappingWidth = 0.0
            fill = textFill
            isManaged = false
        }

        var rectangle: Rectangle? = null

        fun applyHighlight(highlight: Highlight) {
            highlight.style(text)
            if (highlight is FillHighlight) {
                if (rectangle == null) {
                    rectangle = Rectangle(5.0, 5.0).apply {
                        fill = null
                        isSmooth = false
                        styleClass.add("rectangle")
                        isManaged = false
                    }
                }
                highlight.style(rectangle!!)
            }
        }

        fun position(x: Double, y: Double): Double {
            text.applyCss()
            text.layoutX = x
            text.layoutY = y
            val textBounds = text.boundsInLocal

            rectangle?.let {
                with(it) {
                    applyCss()
                    layoutX = x
                    layoutY = y + textBounds.minY
                    width = textBounds.width
                    height = textBounds.height
                }
            }
            return textBounds.width
        }
    }

    private inner class CaretAnimation {

        private val animation = Timeline().apply {
            cycleCount = Timeline.INDEFINITE

            keyFrames.addAll(
                    KeyFrame(Duration.ZERO, EventHandler<ActionEvent> { blinkOn = true }),
                    KeyFrame(Duration.seconds(0.5), EventHandler<ActionEvent> { blinkOn = false }),
                    KeyFrame(Duration.seconds(1.0))
            )
        }

        private var blinkOn: Boolean = false
            set(v) {
                field = v
                caret.opacity = if (caret.isVisible && v) 1.0 else 0.0
            }

        init {
            if (caret.isVisible) animation.play()
            // Stop and start the animation
            caret.visibleProperty().addListener { _, _, newValue ->
                if (newValue == true) {
                    blinkOn = true
                    animation.play()
                } else {
                    animation.stop()
                    blinkOn = false
                }
            }
        }
    }


    override fun getCssMetaData(): List<CssMetaData<out Styleable, *>> {
        return getClassCssMetaData()
    }

    //---------------------------------------------------------------------------
    // Companion Object
    //---------------------------------------------------------------------------

    companion object {

        val TEXT_FILL = createPaintCssMetaData<Scarea>("-fx-text-fill") { it.scareaSkin!!.textFillProperty }
        val HIGHLIGHT_FILL = createPaintCssMetaData<Scarea>("-fx-highlight-fill") { it.scareaSkin!!.highlightFillProperty }
        val HIGHLIGHT_TEXT_FILL = createPaintCssMetaData<Scarea>("-fx-highlight-text-fill") { it.scareaSkin!!.highlightTextFillProperty }
        val CURRENT_LINE_FILL = createPaintCssMetaData<Scarea>("-fx-current-line-fill") { it.scareaSkin!!.currentLineFillProperty }

        val STYLEABLES = listOf(
                TEXT_FILL, HIGHLIGHT_FILL, HIGHLIGHT_TEXT_FILL, CURRENT_LINE_FILL
        )

        fun getClassCssMetaData() = STYLEABLES

    }
}

