/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea

import javafx.beans.property.IntegerProperty
import javafx.beans.property.ReadOnlyBooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.value.ObservableStringValue
import java.lang.ref.WeakReference
import java.util.*


class ScareaDocument(text: String) {

    /**
     * A mutable version of [paragraphs]. These should ONLY be changed within [Change] objects.
     * In fact, [Replacement] is probably the only place this is used!
     */
    private val paragraphsP = mutableListOf<String>().apply {
        addAll(text.split("\n"))
    }

    /**
     * A read-only list of strings, which makes up the document's text.
     *
     * Each entry in the list is a line of text without the newline character, so
     * you can think of the newlines as separators between the list items.
     * Note, an empty document is represented by a list of size 1, containing an empty string.
     * An empty list is an illegal state.
     */
    val paragraphs: List<String> = Collections.unmodifiableList(paragraphsP)

    private val indentSizeProperty = SimpleIntegerProperty(4)
    fun indentSizeProperty(): IntegerProperty = indentSizeProperty
    var indentSize: Int
        get() = indentSizeProperty.value
        set(v) {
            indentSizeProperty.value = v
        }

    /**
     * A mutable version of [ranges].
     */
    private val rangesP = mutableListOf<HighlightRange>()

    /**
     * A read-only list of [HighlightRange]s.
     */
    val ranges: List<HighlightRange> = Collections.unmodifiableList(rangesP)

    private val listeners = mutableListOf<WeakReference<ScareaListener>>()

    val history = History()

    /**
     * A representation of the document as a single [String].
     * This is a calculated value, which is expensive in memory and time, so use it sparingly.
     *
     * Consider using [insert], [delete] and [replace] to modify a document, and
     * [substring], [part] or [paragraphs] to read part of a document.
     */
    var text: String
        get() = paragraphsP.joinToString("\n")
        set(v) {
            history.makeChange(Replacement(ScareaPosition.START, paragraphs, v.split("\n")))
        }
    private val textProperty = object : UncachedStringBinding() {
        override fun getValue() = text
    }

    fun textProperty(): ObservableStringValue = textProperty


    val lineCount: Int
        get() = paragraphs.size


    /**
     * The position of the end of the document. [Replacement] updates this during each undo/redo.
     * We could calculate it each time it is needed, but for efficiency, we cache the value.
     */
    private var endPosition = ScareaPosition(paragraphs.size - 1, paragraphs.last().length)


    /**
     * clamps the position within the valid range [ScareaPosition.START] .. [endPosition].
     * If the column was invalid, it is clamped between 0 and the line's length.
     * This does NOT adjust the line due to an invalid column.
     *
     * e.g. safe( Position( 1, -1 ) returns Position( 1, 0 )
     *
     * See also
     */
    fun safe(position: ScareaPosition): ScareaPosition {
        val clamped = position.clamp(ScareaPosition.START, endPosition)

        return if (clamped.column < 0) {
            ScareaPosition(clamped.line, 0)
        } else if (clamped.column > paragraphs[clamped.line].length) {
            ScareaPosition(clamped.line, paragraphs[clamped.line].length)
        } else {
            clamped
        }
    }

    fun endPosition() = endPosition

    fun isEmpty() = paragraphs.size == 1 && paragraphs[0].isEmpty()

    /**
     * Takes a [Position], and returns a new Position after adding [columns].
     * If this takes it beyond the start or end of the line, then the line is adjusted too.
     *
     * The returned position's line is clamped within the range 0..paragraphs.size -1.
     *
     * The returned position's column is clamped within the range 0..paragraph's string length.
     */
    fun addColumns(position: ScareaPosition, columns: Int): ScareaPosition {
        var l = position.line
        var c = position.column + columns
        while (c < 0) {
            if (l <= 0) return ScareaPosition.START
            l--
            c += paragraphs[l].length + 1
        }
        while (c > paragraphs[l].length) {
            c -= paragraphs[l].length + 1
            if (l >= paragraphs.size - 1) {
                return ScareaPosition(paragraphs.size - 1, paragraphs.last().length)
            }
            l++
        }
        return ScareaPosition(l, c)
    }

    /**
     * Returns a [ScareaPosition] for a an index into the document as a [text] String.
     *
     * Note, this is relatively slow (O(n), where n is the number of characters in the document).
     * So using [ScareaPosition] directly is encouraged where possible.
     */
    fun positionForIndex(index: Int): ScareaPosition {
        var line = 0
        var done = 0
        for (str in paragraphs) {
            done += str.length + 1 // 1 for the newline character
            if (done >= index) {
                return ScareaPosition(line, done - index)
            }
            line++
        }
        return endPosition
    }


    /**
     * Lets you listen to changes in the [ScareaDocument].
     *
     * Note, to avoid memory leaks, [WeakReference]s are used, so you MUST keep a reference to your listener.
     * If you find that your stop receiving events, then your listener may have been garbage collected.
     *
     * FYI, Scarea and ScareaSkin both have their own [ScareaListener].
     */
    fun addListener(listener: ScareaListener) {
        listeners.add(WeakReference(listener))
    }

    /**
     * As we use [WeakReference]s, you do not HAVE to remove your listeners (though it is obviously encouraged!)
     */
    fun removeListener(listener: ScareaListener) {
        listeners.removeAll { it.get() == listener }
    }

    fun delete(from: ScareaPosition, to: ScareaPosition) {
        val safeFrom = safe(from)
        val safeTo = safe(to)

        history.makeChange(Replacement(safeFrom, part(safeFrom, safeTo), listOf("")))
    }

    fun insert(from: ScareaPosition, newText: String) {
        val safeFrom = safe(from)
        history.makeChange(Replacement(safeFrom, listOf(""), newText.split("\n")))
    }

    fun replace(from: ScareaPosition, to: ScareaPosition, newText: String) {
        if (from > to) {
            replace(to, from, newText)
            return
        }
        val safeFrom = safe(from)
        val safeTo = safe(to)

        history.makeChange(Replacement(safeFrom, part(safeFrom, safeTo), newText.split("\n")))
    }

    fun addHighlightRange(hr: HighlightRange) {
        addHighlightRanges(listOf(hr))
    }

    fun addHighlightRanges(ranges: List<HighlightRange>) {
        // Transient ranges do NOT go through the history mechanism. They cannot be undone.
        val transient = ranges.filter { it.transient }
        if (transient.isNotEmpty()) {
            AddHighlightRanges(transient).redo()
        }
        // Permanent ranges (non-transient) DO go though the history mechanism.
        val permanent = ranges.filter { !it.transient }
        if (permanent.isNotEmpty()) {
            history.makeChange(AddHighlightRanges(ranges))
        }
    }

    fun removeHighlightRange(hr: HighlightRange) {
        removeHighlightRanges(listOf(hr))
    }

    fun removeHighlightRanges(ranges: List<HighlightRange>) {
        // Transient ranges do NOT go through the history mechanism. They cannot be undone.
        val transient = ranges.filter { it.transient }
        if (transient.isNotEmpty()) {
            RemoveHighlightRanges(transient).redo()
        }
        // Permanent ranges (non-transient) DO go though the history mechanism.
        val permanent = ranges.filter { !it.transient }
        if (permanent.isNotEmpty()) {
            history.makeChange(RemoveHighlightRanges(ranges))
        }
    }

    fun removeRangesByOwner(owner: Any?) {
        removeHighlightRanges(ranges.filter { it.owner === owner })
    }

    fun clearHighlightRanges() {
        removeHighlightRanges(ranges.toList())
    }

    fun charAt(position: ScareaPosition): Char {
        val str = paragraphs[position.line]
        return if (position.column == str.length) '\n' else str[position.column]
    }

    fun substring(from: ScareaPosition) = substring(from, endPosition())

    fun substring(from: ScareaPosition, to: ScareaPosition): String {

        return if (from < to) {
            val safeFrom = safe(from)
            val safeTo = safe(to)

            if (safeFrom.line == safeTo.line) {
                paragraphsP[safeFrom.line].substring(safeFrom.column, safeTo.column)
            } else {
                val buffer = StringBuffer()
                // The end part of the "from" paragraph
                buffer.append(paragraphsP[safeFrom.line].substring(safeFrom.column))
                buffer.append('\n')
                // All of the in between paragraphs
                for (l in safeFrom.line + 1 until safeTo.line) {
                    buffer.append(paragraphsP[l])
                    buffer.append('\n')
                }
                // The start of the "to" paragraph
                buffer.append(paragraphsP[safeTo.line].substring(0, safeTo.column))

                buffer.toString()
            }
        } else {
            ""
        }
    }

    fun part(from: ScareaPosition, to: ScareaPosition): List<String> {
        val safeFrom = safe(from)
        val safeTo = safe(to)

        return if (safeFrom < safeTo) {
            if (safeFrom.line == safeTo.line) {
                listOf(paragraphsP[safeFrom.line].substring(safeFrom.column, safeTo.column))
            } else {
                val result = mutableListOf<String>()
                // The end part of the first line
                result.add(paragraphsP[safeFrom.line].substring(safeFrom.column))

                // All of the in between paragraphs
                for (l in safeFrom.line + 1 until safeTo.line) {
                    result.add(paragraphsP[l])
                }
                // The start of the "to" paragraph
                result.add(paragraphsP[safeTo.line].substring(0, safeTo.column))

                result
            }
        } else {
            listOf("")
        }
    }

    /**
     * Given that text at [from]..[removedTo] has just be replaced with text at [from]..[to], then what is the
     * new ScareaPosition for [position]?
     */
    fun adjustPosition(
        position: ScareaPosition,
        from: ScareaPosition,
        to: ScareaPosition,
        removedTo: ScareaPosition
    ): ScareaPosition {
        return if (position < from) {
            // Before the replacement, therefore position is unchanged.
            position
        } else if (position <= removedTo) {
            // Was in the deleted portion of the replacement, so use [to]
            to
        } else if (position.line > removedTo.line) {
            // A later line (so the column should stay the same)
            ScareaPosition(position.line - removedTo.line + to.line, position.column)
        } else {
            // Must be on the same line as removeTo, the new line should be the [to] line, and the column is calculated based on [position], [to] and [removedTo]
            ScareaPosition(to.line, position.column - removedTo.column + to.column)
        }
    }

    /**
     * Stores the Undo/Redo history for a [ScareaDocument].
     * See [ScareaDocument.history].
     */
    inner class History {

        private val history = mutableListOf<ScareaDocument.Change>()

        /**
         * The index within [history] for the next [Change].
         * It is 0 when there is nothing to undo, and history.size when there is nothing to redo.
         */
        private var index = 0

        /**
         * Set to [index] when [beginBatch] is called, and reset to -1 after the matching [endBatch].
         */
        private var batchStartIndex = -1

        /**
         * A number of changes can be added to the history as a single batch, so that undo/redo will undo/redo
         * all of the changes in the batch. For example a Find and Replace tool would use batches when performing
         * "Replace All", so that only one "undo" is needed to revert all of the replacements.
         *
         * Each [beginBatch] must have a matching [endBatch], with as many [makeChange] calls as needed.
         * Batches cannot be nested, and you cannot [undo] or [redo] within a [beginBatch] / [endBatch] pair.
         *
         * Note, changes to the document are made when [makeChange] is called.
         *
         * If you need to abort a batch half way through, end the batch, and perform an [undo].
         */
        fun beginBatch() {
            if (batchStartIndex >= 0) throw IllegalStateException("Already within a batch")
            batchStartIndex = index
        }


        fun endBatch() {
            if (batchStartIndex < 0) throw IllegalStateException("Unmatched endBatch")
            if (index != history.size) throw IllegalStateException("Cannot end a batch when redoable changes are present") // Impossible to get here!?
            val subset = history.subList(batchStartIndex, index)
            val batch = BatchChange(subset.toList())
            subset.clear()
            history.add(batch)
            index = history.size

            batchStartIndex = -1
        }

        fun makeChange(change: Change) {
            //TODO Remove
            if (change is RemoveHighlightRanges && change.items.isEmpty()) Thread.dumpStack()
            if (change is AddHighlightRanges && change.items.isEmpty()) Thread.dumpStack()

            while (canRedo()) {
                history.removeAt(history.size - 1)
            }
            // See if this change can be merged with the previous one.
            // For example, typing a few characters will generate multiple Change objects
            // which should be merged, so that undo deletes all of them in one go.
            val merged = if (index <= 0) {
                null
            } else {
                change.mergeWith(history[index - 1])
            }
            if (merged == null) {
                history.add(change)
                index = history.size
            } else {
                history[index - 1] = merged
            }
            change.redo()

            while (history.size > maxHistory) {
                history.removeAt(0)
                index--
            }
            update()
        }

        /**
         * The maximum number of items stored in the [history].
         * When the history exceeds this value, older items cannot be undone.
         */
        val maxHistory = 100

        private val undoableProperty = SimpleBooleanProperty(false)
        fun undoableProperty(): ReadOnlyBooleanProperty = undoableProperty

        private val redoableProperty = SimpleBooleanProperty(false)
        fun redoableProperty(): ReadOnlyBooleanProperty = redoableProperty

        private fun update() {
            undoableProperty.value = canUndo()
            redoableProperty.value = canRedo()
        }

        fun clear() {
            history.clear()
            index = 0
        }

        fun canUndo() = index > 0

        fun canRedo() = index < history.size

        fun undo() {
            if (batchStartIndex >= 0) throw IllegalStateException("Cannot undo within a batch")

            if (canUndo()) {
                history[index - 1].undo()
                index--
            }
            update()
        }

        fun redo() {
            if (batchStartIndex >= 0) throw IllegalStateException("Cannot redo within a batch")

            if (canRedo()) {
                history[index].redo()
                index++
            }
            update()
        }

    }

    /**
     * A item in the undo/redo history.
     * All changes to the [ScareaDocument] are performed using a [Change].
     * Altering the [ScareaDocument] outside of a [Change] will cause unpredictable behaviour.
     */
    interface Change {
        fun undo()
        fun redo()

        /**
         * If the two changes cannot be merged, then return null.
         */
        fun mergeWith(older: Change): Change?
    }

    private class BatchChange(val changes: List<Change>) : Change {
        override fun undo() {
            for (i in changes.size - 1 downTo 0) {
                changes[i].undo()
            }
        }

        override fun redo() {
            for (c in changes)
                c.redo()
        }

        override fun mergeWith(older: Change): Change? = null
    }

    private fun replaced(from: ScareaPosition, to: ScareaPosition, removedTo: ScareaPosition) {
        textProperty.invalidate()
        for (wl in listeners) {
            wl.get()?.replaced(this, from, to, removedTo)
        }
        listeners.removeAll { it.get() == null }
    }

    private fun rangesRemoved(ranges: List<HighlightRange>) {
        for (wl in listeners) {
            wl.get()?.rangesRemoved(this, ranges)
        }
        listeners.removeAll { it.get() == null }
    }

    private fun rangesAdded(ranges: List<HighlightRange>) {
        for (wl in listeners) {
            wl.get()?.rangesAdded(this, ranges)
        }
        listeners.removeAll { it.get() == null }
    }

    /**
     * Used internally to store the changes made to a [HighlightRange] during a [Replacment].
     */
    private class AlteredRange(
        val range: HighlightRange,
        val oldFrom: ScareaPosition,
        val oldTo: ScareaPosition,
        var newFrom: ScareaPosition,
        var newTo: ScareaPosition
    )

    /**
     * [ScareaDocument.insert], [ScareaDocument.delete] and [ScareaDocument.replace] all create [Replacement]s.
     */
    private inner class Replacement(
        val position: ScareaPosition,
        val oldText: List<String>,
        val newText: List<String>

    ) : Change {

        /**
         * If two changes are made in rapid succession, they can be merged into a single [Replacement].
         */
        private val time = System.currentTimeMillis()

        private var firstRedo = true

        private val alteredRanges = mutableListOf<AlteredRange>()


        /**
         * Looks at all of the [ranges], and adjust their from/to positions if this change
         * affects them.
         *
         * If [HighlightRange.transient], then simply make the change, but if not transient, then
         * make a note of how the range was changed, so that redo can reverse it. This is only done
         * when [firstRedo] == true.
         */
        private fun updateRanges(from: ScareaPosition, removedTo: ScareaPosition, addedTo: ScareaPosition) {

            for (r in ranges) {

                fun alterRange(from: ScareaPosition, to: ScareaPosition) {
                    if (!r.transient) {
                        if (firstRedo) {
                            alteredRanges.add(AlteredRange(r, r.from, r.to, from, to))
                        }
                    } else {
                        r.fromP = from
                        r.toP = to
                    }
                }

                /**
                 * Given [pos], how should it be adjusted so that it refers to the same part of the document
                 * after the replacement has occurred. (Based on [removedTo] and [addedTo].
                 */
                fun adjust(pos: ScareaPosition): ScareaPosition {
                    return if (pos.line == removedTo.line) {
                        if (pos.line == addedTo.line) {
                            // All on the same line
                            ScareaPosition(pos.line, pos.column + addedTo.column - removedTo.column)
                        } else {
                            // pos is the same line as removeTo, but not the same line as addedTo
                            ScareaPosition(addedTo.line, addedTo.column + pos.column - removedTo.column)
                        }
                    } else {
                        // pos is not on the same line as removeTo, so we just need to adjust the line
                        ScareaPosition(pos.line + addedTo.line - removedTo.line, pos.column)
                    }
                }

                if (r.from == from && r.to == removedTo) {
                    // The range is the same size as the deletion, so make it the size of the addition.
                    alterRange(r.from, addedTo)
                } else if (r.from > from && r.to < removedTo) {
                    // The range is completely within the deleted part
                    alterRange(r.from, r.from) // Make is zero size
                } else if (r.to == from) {
                    // The range ends where the replacement starts
                    if (r.expand && removedTo == from) {
                        // Added (no deletion), Include the new text as part of the range
                        alterRange(r.from, addedTo)
                    }
                } else if (r.to < from) {
                    // Before, so no change
                } else if (r.from >= removedTo) {
                    // Range is after the replacement, so adjust the start and end by the same amount.
                    alterRange(adjust(r.from), adjust(r.to))
                } else if (r.from < position && r.to >= removedTo) {
                    // The replaced section is completely inside the range, so increase the range's to.
                    alterRange(r.from, adjust(r.to))
                } else if (removedTo > r.from && removedTo < r.to) {
                    // Cut the front off of the range
                    alterRange(addedTo, adjust(r.to))
                } else if (position < r.to) {
                    // Cut the end off of the range
                    alterRange(r.from, position)
                } else {
                    println("Hmm, range=${r.from}..${r.to} and removed ${from}..${removedTo}")
                }

            }
        }

        private fun delete(lines: List<String>) {
            require(lines.isNotEmpty())

            if (lines.size == 1) {
                if (lines.first().isEmpty()) return
                val old = paragraphsP[position.line]
                paragraphsP[position.line] =
                    old.substring(0, position.column) + old.substring(position.column + lines.first().length)
            } else {
                // Cut off the end of the first line, and join on the end of the last line
                paragraphsP[position.line] =
                    paragraphsP[position.line].substring(0, position.column) +
                            paragraphsP[position.line + lines.size - 1].substring(lines.last().length)

                // Delete the remaining lines
                paragraphsP.subList(position.line + 1, Math.min(position.line + lines.size, paragraphsP.size)).clear()
            }
            // I don't think it is possible to delete all the paragraphs, but just in case!
            if (paragraphsP.isEmpty()) {
                println("WARNING paragraphs were empty")
                paragraphsP.add("")
            }

            endPosition = ScareaPosition(paragraphsP.size - 1, paragraphsP.last().length)
        }

        private fun add(lines: List<String>) {
            require(lines.isNotEmpty())

            if (lines.size == 1) {
                if (lines.first().isEmpty()) return

                val old = paragraphsP[position.line]
                paragraphsP[position.line] =
                    old.substring(0, position.column) +
                            lines.first() +
                            old.substring(position.column)


            } else {
                // Append the first line
                val old = paragraphsP[position.line]
                val oldEnd = old.substring(position.column) // This will be part of the last line...
                paragraphsP[position.line] = old.substring(0, position.column) + lines.first()

                // Add the middle lines
                if (lines.size > 2) {
                    paragraphsP.addAll(position.line + 1, lines.subList(1, lines.size - 1))
                }

                // Add the final line
                paragraphsP.add(position.line + lines.size - 1, lines.last() + oldEnd)
            }

            endPosition = ScareaPosition(paragraphsP.size - 1, paragraphsP.last().length)
        }

        private fun toPosition(lines: List<String>): ScareaPosition {
            return if (lines.size == 1) {
                ScareaPosition(position.line, position.column + lines.first().length)
            } else {
                ScareaPosition(position.line + lines.size - 1, lines.last().length)
            }
        }


        override fun redo() {
            delete(oldText)
            add(newText)

            val removedTo = toPosition(oldText)
            val addedTo = toPosition(newText)
            replaced(position, addedTo, removedTo)

            updateRanges(position, removedTo, addedTo)
            firstRedo = false
            // Update ranges will NOT change non-transient ranges, so let's do those now...
            alteredRanges.forEach {
                it.range.fromP = it.newFrom
                it.range.toP = it.newTo
            }
        }

        override fun undo() {
            delete(newText)
            add(oldText)

            val removedTo = toPosition(newText)
            val addedTo = toPosition(oldText)
            replaced(position, addedTo, removedTo)

            updateRanges(position, removedTo, addedTo)
            // Update ranges will NOT change non-transient ranges, so let's do those now...
            alteredRanges.forEach {
                it.range.fromP = it.oldFrom
                it.range.toP = it.oldTo
            }
        }

        /**
         * When two Replacement Changes are merged, we also need to merge the changes to non-transient HighlightRanges.
         */
        private fun mergeAlteredRanges(older: Replacement, newer: Replacement): Replacement {
            alteredRanges.addAll(older.alteredRanges)
            val mapped = mutableMapOf<HighlightRange, AlteredRange>()
            for (ar in alteredRanges) {
                mapped[ar.range] = ar
            }
            for (nar in newer.alteredRanges) {
                val existing = mapped[nar.range]
                if (existing == null) {
                    alteredRanges.add(nar)
                } else {
                    existing.newTo = nar.newTo
                    existing.newFrom = nar.newFrom
                }
            }
            firstRedo = false
            return this
        }

        override fun mergeWith(older: Change): Change? {
            if (older !is Replacement) return null

            if (time - older.time > 1000) return null // Too long a delay for them to be merged.

            fun merge(a: List<String>, b: List<String>): List<String> {
                val result = ArrayList<String>(a.size + b.size)
                result.addAll(a.subList(0, a.size - 1))
                result.add(a.last() + b.first())
                result.addAll(b.subList(1, b.size))
                return result
            }

            fun isNothing(a: List<String>) = a.size == 1 && a.first().isEmpty()

            // Two insertions? The older change may optionally have deleted text. This change cannot have deleted text.
            if (this.position == older.toPosition(older.newText) && isNothing(oldText)) {
                return Replacement(older.position, older.oldText, merge(older.newText, newText)).mergeAlteredRanges(
                    older,
                    this
                )
            }

            // Two deletions? (e.g. press the delete key twice)
            if (isNothing(newText) && isNothing(older.newText) && position == older.position) {
                return Replacement(older.position, merge(older.oldText, oldText), newText).mergeAlteredRanges(
                    older,
                    this
                )
            }
            // Two deletions (but in the opposite direction to the above (e.g.press backspace twice)
            if (isNothing(newText) && isNothing(older.newText) && toPosition(oldText) == older.position) {
                return Replacement(position, merge(oldText, older.oldText), newText).mergeAlteredRanges(older, this)
            }

            return null
        }

        override fun toString() = "Replacement @ $position = $newText"
    }

    private inner class AddHighlightRanges(
        val items: List<HighlightRange>
    ) : Change {

        override fun mergeWith(older: Change): Change? = null

        override fun redo() {
            rangesP.addAll(items)
            rangesAdded(items)
        }

        override fun undo() {
            rangesP.removeAll(items)
            rangesRemoved(items)
        }

        override fun toString() = "AddHighlightRanges(count=${items.size})"
    }

    private inner class RemoveHighlightRanges(
        items: List<HighlightRange>
    ) : Change {

        val items = items.filter { rangesP.contains(it) }

        override fun mergeWith(older: Change): Change? = null

        override fun redo() {
            rangesP.removeAll(items)
            rangesRemoved(items)
        }

        override fun undo() {
            rangesP.addAll(items)
            rangesAdded(items)
        }

        override fun toString() = "RemoveHighlightRanges(count=${items.size})"

    }

}
