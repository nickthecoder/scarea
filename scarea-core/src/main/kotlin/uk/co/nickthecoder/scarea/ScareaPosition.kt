/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea

import java.util.*

/**
 * Represents a position within a [ScareaDocument] using a [line] number, and a [column] number.
 *
 * Both [line] and [column] are zero based, so the start position of the document is Position(0,0).
 *
 * Unlike JavaFX's TextArea, a [ScareaDocument] is stored as a list of strings, rather than one
 * (potentially huge String), and therefore using a [ScareaDocument] is the preferred, and most efficient
 * way to describe a position within the document.
 */
class ScareaPosition(val line: Int, val column: Int)
    : Comparable<ScareaPosition> {

    /**
     * Simply subtracts the two lines as well as the two columns. No attempt is made to adjust the line
     * if the new column is negative.
     */
    operator fun minus(other: ScareaPosition) = ScareaPosition(line - other.line, column - other.column)

    /**
     * Simply add the two lines as well as the two columns. No attempt is made to adjust the line
     * if the new column is too high.
     */
    operator fun plus(other: ScareaPosition) = ScareaPosition(line + other.line, column + other.column)

    fun clamp(min: ScareaPosition, max: ScareaPosition) = if (this < min) min else if (this > max) max else this

    override fun compareTo(other: ScareaPosition): Int {
        return if (line == other.line) {
            column - other.column
        } else {
            line - other.line
        }
    }

    override fun equals(other: Any?): Boolean {
        return if (other is ScareaPosition) {
            line == other.line && column == other.column
        } else {
            false
        }
    }

    override fun hashCode() = Objects.hash(line, column)

    /**
     * Print the position using 1 based lines and columns. i.e. the start of the document is (1,1).
     */
    override fun toString() = "(${line + 1},${column + 1})"

    companion object {
        val START = ScareaPosition(0, 0)
    }
}
