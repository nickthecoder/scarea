/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea

/**
 * A range of text which appears different, e.g. a different foreground/background color.
 *
 * Ranges can be [transient], which means they are NOT part of the undo/redo history.
 * An automatic syntax highlighter would use transient ranges.
 *
 * Ranges which are NOT transient will be stored in the undo/redo history, and should be used when the
 * highlight is integral to the document. For example, if you want to make a word bold, then it would not
 * be transient.
 *
 * [transient] determines if this HighlightRange is excluded from the undo/redo history.
 * Ranges used for syntax highlighting, as well as search results should be transient.
 * If you have a styled text document, were the styles are part of the document, then transient should be false.
 * I never use [transient] = false, as I do not use it for styled documents, only as a source code editor.
 *
 * [stretchy] determines what should happen when text is added at the end of this range. This should probably
 * always be false (the default) for transient ranges, such as syntax highlighting.
 * However, if you are writing a styled text document, then to begin writing bold text, create a range with
 * expand=true, then add the text, then reset expand=false when you wish to return adding un-styled text.
 *
 * [owner] can be any object of your choosing (or null), and exists solely so that it is easy to remove (or adjust)
 * all of the ranges owned by a single object without having to keep your own copy of the ranges.
 * For example, a syntax highlighter would set the owner, and then whenever the document is re-highlighted, the
 * existing ranges can be deleted via [TextDocument.removeHighlightRanges].
 */
open class HighlightRange(
        internal var fromP: ScareaPosition,
        internal var toP: ScareaPosition,
        val highlight: Highlight,
        val transient: Boolean = true,
        val expand: Boolean = false,
        val owner: Any? = null
) {
    val from: ScareaPosition
        get() = fromP

    val to: ScareaPosition
        get() = toP

    fun overlaps(other: HighlightRange): Boolean {
        return to > other.from && from < other.to
    }

}
