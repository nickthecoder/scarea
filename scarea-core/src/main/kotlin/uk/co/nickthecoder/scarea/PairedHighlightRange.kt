package uk.co.nickthecoder.scarea

/**
 * A highlight range, which is part of a matched pair, for example, an opening and closing bracket.
 *
 * Always create [PairedHighlightRange] in pairs, never alone, or with more than two items.
 *
 * Note, Scarea knows nothing of PairedHighlightRanges, and will happily remove one of them from its
 * list, leaving the other untouched.
 */
class PairedHighlightRange(
        start: ScareaPosition,
        end: ScareaPosition,
        highlight: Highlight,
        transient: Boolean = true,
        expand: Boolean = false,
        owner: Any? = null,
        opening: PairedHighlightRange?

) : HighlightRange(start, end, highlight, transient = transient, expand = expand, owner = owner) {

    /*
     * Note, because of a chicken and egg problem, we cannot have a simple 'val' for [other].
     * Instead, opening = null for the first of the pair, and then the second instance
     * will assign BOTH of the [other]s in init.
     */
    private lateinit var other: PairedHighlightRange


    val pairedWith: PairedHighlightRange
        get() = other

    init {
        if (opening != null) {
            other = opening
            opening.other = this
        }
    }

}
