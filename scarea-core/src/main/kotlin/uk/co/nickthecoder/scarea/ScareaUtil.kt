/*
Scarea
Copyright (C) 2019 Nick Robinson

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 only, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.scarea

import javafx.beans.binding.IntegerBinding
import javafx.beans.value.ObservableIntegerValue
import javafx.css.*
import javafx.scene.control.ScrollBar
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.text.Font

fun clamp(v: Int, from: Int, to: Int) = Math.max(Math.min(v, to), from)
fun clamp(v: Double, from: Double, to: Double) = Math.max(Math.min(v, to), from)

fun <T : Comparable<T>> min(a: T, b: T) = if (a <= b) a else b
fun <T : Comparable<T>> max(a: T, b: T) = if (a >= b) a else b

val javaFXVersion: String? by lazy { System.getProperties().getProperty("javafx.runtime.version") }

private fun getOs(): String = System.getProperty("os.name").toLowerCase()

val isMac by lazy { getOs().startsWith("mac") }
val isLinux by lazy { getOs().startsWith("linux") }
val isWindows by lazy { getOs().startsWith("windows") }

/**
 * If [ScrollBar.max] is the total height of the document, and [ScrollBar.visibleAmount] is the viewport's height,
 * then [ScrollBar.value] is NOT the offset we need to apply to the view's contents, because [ScrollBar.value] ranges
 * from 0 to [ScrollBar.max].
 *
 * For example, if the document is 100 high, and the viewport 10, then this will return a value in the range 0..90,
 * whereas [ScrollBar.value] returns a value in the range 0..100.
 *
 * Note, this assumes that [ScrollBar.min] == 0.0
 */
var ScrollBar.safeValue
    get() = clamp(value, 0.0, max)
    set(v) {
        value = clamp(v, 0.0, max)
    }

/**
 * Sets the scrollbars max and visible values.
 * If we have a document with 10 lines of text, and only 10 are visible, you may think that [ScrollBar.max] = 100 and
 * [ScrollBar.visibleAmount] = 10, but that is wrong! Because then the scroll bar's [ScrollBar.value] would range from 0..100.
 * We need the range to be 0..90.
 *
 * So [ScrollBar.max] = [total]-[viewed]
 *
 * This also sets the
 */
fun ScrollBar.setBounds(total: Double, viewed: Double) {
    max = Math.max(1.0, total - viewed)
    visibleAmount = viewed / total * max
    isVisible = viewed < total
    if (!isVisible) value = 0.0
    value = clamp(value, min, max)
}

/**
 * Useful for converting a zero-based index into a one-based index>
 * For example, Scarea uses zero based indices for lines and columns,
 * and this will convert them to human friendly versions.
 */
fun ObservableIntegerValue.incremented(): IntegerBinding = object : IntegerBinding() {
    init {
        bind(this@incremented)
    }

    override fun computeValue() = this@incremented.get() + 1
}

//--------------------------------------------------------------------------
// Helps reduce CSS Boiler plate cruft
//--------------------------------------------------------------------------

inline fun <reified N : Styleable> createPaintCssMetaData(
        property: String,
        defaultValue: Paint = Color.WHITE,
        crossinline getter: (N) -> StyleableObjectProperty<Paint?>) =
        createCssMetaData<N, Paint?>(property, StyleConverter.getPaintConverter(), defaultValue, getter)

inline fun <reified N : Styleable> createBooleanCssMetaData(
        property: String,
        defaultValue: Boolean = false,
        crossinline getter: (N) -> StyleableBooleanProperty) =
        object : CssMetaData<N, Boolean>(property, StyleConverter.getBooleanConverter(), defaultValue) {
            override fun getStyleableProperty(styleable: N) = getter(styleable)
            override fun isSettable(styleable: N): Boolean = !getStyleableProperty(styleable).isBound
        }

inline fun <reified N : Styleable> createFontCssMetaData(
        property: String,
        crossinline getter: (N) -> StyleableObjectProperty<Font>) =
        createCssMetaData<N, Font>(property, StyleConverter.getFontConverter(), Font.getDefault(), getter)


inline fun <reified N : Styleable, T> createCssMetaData(
        property: String,
        converter: StyleConverter<*, T>,
        defaultValue: T,
        crossinline getter: (N) -> StyleableObjectProperty<T>): CssMetaData<N, T> {

    return object : CssMetaData<N, T>(property, converter, defaultValue) {
        override fun getStyleableProperty(styleable: N) = getter(styleable)
        override fun isSettable(styleable: N): Boolean = !getStyleableProperty(styleable).isBound
    }
}

fun <T> Any.createStyleable(name: String, value: T, meta: CssMetaData<out Styleable, T>) = object : StyleableObjectProperty<T>(value) {
    override fun getBean() = this@createStyleable
    override fun getName() = name
    override fun getCssMetaData() = meta
}

fun Any.createStyleable(name: String, value: Boolean, meta: CssMetaData<out Styleable, Boolean>): StyleableBooleanProperty = object : StyleableBooleanProperty(value) {
    override fun getBean() = this@createStyleable
    override fun getName() = name
    override fun getCssMetaData() = meta
}
